# Oasis Import Tool

This tool uses the database with courses from Oasis to generate a set of fixture files that can be imported into the Django application.

## Project setup

1. Install [Node.JS](https://nodejs.org/en/download/) and make sure your install includes [NPM](https://npmjs.com)
2. Install the required dependencies:
    ```sh
    npm install
    ```
3. Copy the `.env.template` file to `.env` and enter the required database configuration.
4. Execute the program for the correct academic year:
    ```sh
    npm run generate -- -y "2021"
    ```

    The fixtures will be outputted to the `out` directory.
