import { BaseEntity, Column, Entity, PrimaryColumn } from 'typeorm'

@Entity({ name: 'faculteiten' })
export class Faculty extends BaseEntity {
	@PrimaryColumn({ name: 'afkorting' })
	id_code: string

	@Column({ name: 'code' })
	code: string

	@Column({ name: 'naam_n' })
	name_en: string

	@Column({ name: 'naam_e' })
	name_nl: string
}
