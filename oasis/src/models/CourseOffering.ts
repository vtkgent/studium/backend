import { BaseEntity, Column, Entity, PrimaryColumn } from 'typeorm'
import { Course } from './Course'

@Entity({ name: 'opl2vakken' })
export class CourseOffering extends BaseEntity {
	@PrimaryColumn({ name: 'opl' })
	program_code: string

	@PrimaryColumn({ name: 'cc' })
	course_code: string

	@PrimaryColumn({ name: 'onderdeel' })
	part_code: string

	@PrimaryColumn({ name: 'sem' })
	semester: string

	@PrimaryColumn({ name: 'sp' })
	credits: number

	@PrimaryColumn({ name: 'mt1' })
	year: string

	@Column({ name: 'aj' })
	academic_year: number
}
