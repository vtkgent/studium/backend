import { BaseEntity, Column, Entity, PrimaryColumn } from 'typeorm'
import { CourseOffering } from './CourseOffering'

@Entity({ name: 'opleidingen' })
export class Program extends BaseEntity {
	@PrimaryColumn({ name: 'opl' })
	code: string

	@Column({ name: 'naam_e' })
	name_en: string

	@Column({ name: 'naam_n' })
	name_nl: string
}
