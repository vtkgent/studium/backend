import { BaseEntity, Column, Entity, PrimaryColumn } from 'typeorm'

@Entity({ name: 'vakken' })
export class Course extends BaseEntity {
	@PrimaryColumn({ name: 'cc' })
	code: string

	@Column({ name: 'naam' })
	name_nl: string

	@Column({ name: 'naam_e' })
	name_en: string

	@Column({ name: 'aj' })
	year: number
}
