import { BaseEntity, Column, Entity, PrimaryColumn } from 'typeorm'

@Entity({ name: 'lesgevers' })
export class Instructor extends BaseEntity {
	@PrimaryColumn({ name: 'lg' })
	code: string

	@PrimaryColumn({ name: 'cc' })
	course_code: string
}
