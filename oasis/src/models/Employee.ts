import { BaseEntity, Column, Entity, PrimaryColumn } from 'typeorm'

@Entity({ name: 'lg2naam' })
export class Employee extends BaseEntity {
	@PrimaryColumn({ name: 'lg' })
	code: string

	@PrimaryColumn({ name: 'naam' })
	name: string
}
