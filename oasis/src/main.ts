import YAML from 'yaml'
import fs from 'fs'
import path from 'path'
import chalk from 'chalk'
import { ArgumentParser } from 'argparse'
import { createConnection } from 'typeorm'
import { Faculty } from './models/Faculty'
import { Program } from './models/Program'
import { Course } from './models/Course'
import { CourseOffering } from './models/CourseOffering'
import { Instructor } from './models/Instructor'
import { Employee } from './models/Employee'

// Output directory for the fixtures
const OUT_DIR = path.join(__dirname, '../out')

async function main(args: any) {
	// Load environment variables from the ".env" file
	require('dotenv').config()

	console.log(chalk.cyan('Connecting to database...'))

	// Create a TypeOrm connection
	const connection = await createConnection({
		// Connection information
		type: 'mysql',
		host: process.env.DB_HOST,
		port: Number(process.env.DB_PORT),
		username: process.env.DB_USER,
		password: process.env.DB_PASS,
		database: process.env.DB_NAME,

		// Models
		entities: [Faculty, Program, Course, CourseOffering, Instructor, Employee],

		// Do not reflect changes of the local scheme to the database.
		synchronize: false,
	})

	// Make the "out" directory if it doesn't exist
	if (!fs.existsSync(OUT_DIR)) {
		console.log(chalk.cyan('Creating fixture output directory...'))
		fs.mkdirSync(OUT_DIR)
	}

	console.log()
	console.log(chalk.blueBright('=> [Employees] Fetching...'))

	// Get all instructors
	const instructors = await Instructor.find()

	// Get all employees
	const employees = await Employee.find()

	console.log(chalk.greenBright(`=> [Employees] Found ${employees.length} employees`))

	// Generate employees fixture object
	const employeesFixture = employees.map((employee, index) => ({
		model: 'app.employee',
		pk: index,
		fields: {
			code: employee.code,
			name: employee.name,
		},
	}))

	// Convert the fixture object to YAML and save
	console.log(chalk.blueBright('=> [Employees] Saving fixture...'))
	fs.writeFileSync(path.join(OUT_DIR, 'employees.yaml'), YAML.stringify(employeesFixture))

	// ----------------------------------------------------------------------------------

	console.log(chalk.blueBright('=> [Faculties] Fetching...'))

	// Get all faculties
	const faculties = await Faculty.find()

	console.log(chalk.greenBright(`=> [Faculties] Found ${faculties.length} faculties`))

	// Generate faculties fixture object
	const facultiesFixture = faculties.map((faculty, index) => ({
		model: 'app.faculty',
		pk: index,
		fields: {
			id_code: faculty.id_code,
			code: faculty.code,
			name_en: faculty.name_en,
			name_nl: faculty.name_nl,
		},
	}))

	// Convert the fixture object to YAML and save.
	console.log(chalk.blueBright('=> [Faculties] Saving fixture...'))
	fs.writeFileSync(path.join(OUT_DIR, 'faculties.yaml'), YAML.stringify(facultiesFixture))

	// ----------------------------------------------------------------------------------

	console.log(chalk.blueBright('=> [Programs] Fetching...'))

	// Get all programs
	const programs = await Program.find()

	console.log(chalk.greenBright(`=> [Programs] Found ${programs.length} programs`))

	// Generate programs fixture object
	const programsFixture = programs.map((program, index) => ({
		model: 'app.program',
		pk: index,
		fields: {
			code: program.code,
			name_en: program.name_en,
			name_nl: program.name_nl,
			// The id_code of the faculty is the first letter of the program code.
			faculty: faculties.findIndex((faculty) => faculty.id_code === program.code[0]),
		},
	}))

	// Convert the fixture object to YAML and save.
	console.log(chalk.blueBright('=> [Programs] Saving fixture...'))
	fs.writeFileSync(path.join(OUT_DIR, 'programs.yaml'), YAML.stringify(programsFixture))

	// ----------------------------------------------------------------------------------

	console.log(chalk.blueBright('=> [Courses] Fetching...'))

	// Get all courses
	const courses = await Course.find({ where: { year: args.year } })

	console.log(chalk.greenBright(`=> [Courses] Found ${courses.length} courses`))

	// Generate courses fixture object
	const coursesFixture = courses.map((course, index) => ({
		model: 'app.course',
		pk: index,
		fields: {
			code: course.code,
			name_en: course.name_en,
			name_nl: course.name_nl,
			// Link all instructors coupled to the course and map to the instructor index
			instructors: instructors
				// Get the instructors for this specific course
				.filter((instructor) => instructor.course_code === course.code)
				// Map to the index (primary key) of the instructor
				.map((instructor) => employees.findIndex((employee) => employee.code === instructor.code))
				// Filter all negative values (instructors that don't exist)
				.filter((instructor) => instructor >= 0),
		},
	}))

	// Convert the fixture object to YAML and save.
	console.log(chalk.blueBright('=> [Courses] Saving fixture...'))
	fs.writeFileSync(path.join(OUT_DIR, 'courses.yaml'), YAML.stringify(coursesFixture))

	// ----------------------------------------------------------------------------------

	console.log(chalk.blueBright('=> [CourseOfferings] Fetching...'))

	// Get all course offerings
	const courseOfferings = await CourseOffering.find({ where: { academic_year: args.year } })

	console.log(
		chalk.greenBright(`=> [CourseOfferings] Found ${courseOfferings.length} course offerings`),
	)

	// Generate course offerings fixture object
	const courseOfferingsFixture = courseOfferings.map((offering, index) => ({
		model: 'app.courseoffering',
		pk: index,
		fields: {
			semester: offering.semester,
			credits: offering.credits,
			year: offering.year,
			// Find the matching course based on the "course_code"
			course: courses.findIndex((course) => course.code === offering.course_code),
			// Find the matching program based on the "program_code"
			program: programs.findIndex((program) => program.code === offering.program_code),
		},
	}))

	// Convert the fixture object to YAML and save.
	console.log(chalk.blueBright('=> [CourseOfferings] Saving fixture...'))
	fs.writeFileSync(
		path.join(OUT_DIR, 'course_offerings.yaml'),
		YAML.stringify(courseOfferingsFixture),
	)

	// ----------------------------------------------------------------------------------

	// Close the connection when done
	console.log(chalk.cyan('Closing database connection...'))
	connection.close()

	console.log(chalk.green('All done!'))
}

// Setup the argparser
const parser = new ArgumentParser({
	description: 'Studiegids Studium fixture generator',
})
parser.add_argument('-y', '--year', {
	help: 'year of the courses to use (eg: 2021 will only get courses with year:2021)',
	required: true,
	type: 'int',
})

const args = parser.parse_args()

// Execute main function
main(args)
