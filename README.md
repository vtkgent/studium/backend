# Studium (backend)

Studium is an online platform for exchanging summaries, tips and learning 
experiences.

Studium is free software. See the files whose names start with COPYING for
copying permission.

Copyright years on Studium source files may be listed using range notation,
e.g., 2020-2025, indicating that every year in the range, inclusive, is a
copyrightable year that could otherwise be listed individually.

## Project setup

### Preparation for Windows

- [Install Windows Terminal](https://docs.microsoft.com/en-us/windows/terminal/get-started) (you don't have to but you might want to)

- [Install WSL 2](https://docs.microsoft.com/en-us/windows/wsl/install-win10)

- [Install Docker](https://docs.docker.com/docker-for-windows/install-windows-home/)

- Proceed to the WSL part below

### Ubuntu or WSL
- Install Git and Docker
  ```shell script
  $ sudo apt update && sudo apt install -y git docker.io docker-compose
  $ sudo systemctl enable --now docker  (not needed on WSL)
  $ sudo usermod -aG docker $USER
  $ newgrp docker
  ```
 
- Clone this repository
  ```shell script
  $ git clone https://gitlab.com/vtkgent/studium/backend.git
  $ pushd backend
  ```

- Install packages inside virtual environment
  ```shell script
  $ python -m venv venv
  $ . venv/bin/activate
  (venv) $ pip install -r requirements.txt
  ```

### Install Pycharm (optional)
- [Apply for a free Jetbrains student license](https://www.jetbrains.com/shop/eform/students)

- [Install Jetbrains Toolbox](https://www.jetbrains.com/toolbox-app/)

- Log in in Jetbrains Toolbox with your student account

- Install Pycharm from the Jetbrains Toolbox

- Open the cloned repository in Pycharm

### Install browser extensions (optional)
- Vue.js Developer Tools ([Chrome](https://chrome.google.com/webstore/detail/vuejs-devtools/nhdogjmejiglipccpnnnanhbledajbpd), [Firefox](https://addons.mozilla.org/en-US/firefox/addon/vue-js-devtools/))

- Apollo Client Developer Tools ([Chrome](https://chrome.google.com/webstore/detail/apollo-client-developer-t/jdkknkkbebbapilgoeccciglkfbmbnfm), [Firefox](https://addons.mozilla.org/en-US/firefox/addon/apollo-developer-tools/))

## Running the server
#### Getting the latest images
```shell script
docker login registry.gitlab.com
docker-compose pull
```

#### Run the backend
```shell script
$ make
```

#### Developing without mocking CAS

Install a proxy to tunnel http://localhost:4000 through ssl (required for CAS)
1. Run `yarn global add local-ssl-proxy`
2. Run `local-ssl-proxy --source=8080 --target=4000` after starting the frontend
3. Browse `https://localhost:8080` (which is allowed by CAS for development)

Disable the CAS mock by overriding it with the UGent CAS server
```sh
# .env
DJANGO_CAS_SERVER_URL=https://login.ugent.be
```

### Running a Django command
```shell script
$ alias backend='docker-compose exec backend python manage.py'
$ backend <command>
```

##### Creating a superuser
```shell script
$ backend createsuperuser
```

##### Making migrations
```shell script
$ backend makemigrations
```

##### Making translation files
```shell script
$ backend makemessages
or
$ backend makemessages --locale en-us
```

##### Entering the Python shell
```shell script
$ backend shell_plus
```

##### Providing test data
```shell script
$ backend initdb
```

##### Scraping the course catalog
```shell script
$ backend scrape
```
