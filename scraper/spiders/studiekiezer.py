import logging
import re

import scrapy
from dateutil.relativedelta import relativedelta
from django.utils import timezone
from scrapy import FormRequest

from scraper.items import (
    CourseItem,
    CourseOfferingItem,
    EmployeeItem,
    FacultyItem,
    ProgramItem,
)

logger = logging.getLogger("main")


class StudiekiezerSpider(scrapy.Spider):
    name = "studiekiezer"
    start_url = "https://studiekiezer.ugent.be/%(lang)s/zoek?aj=%(year)d&taal=%(lang)s"
    search_url = "https://studiekiezer.ugent.be/%(lang)s/incrementalsearch?target=zoek&ids=%(chunks)s"

    def __init__(self, year=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # Default to current academic year (1st of September)
        if not year:
            start = relativedelta(months=8)
            year = (timezone.now() - start).year
        self.year = year

    def start_requests(self):
        """Start requests"""
        logger.info(
            "Studiekiezer scraper has started. Scraping everything may take a long time."
        )
        # Parse English version of the website
        yield scrapy.Request(
            url=self.start_url % {"lang": "EN", "year": self.year},
            callback=self.parse,
            meta={"lang": "en"},
        )
        # Parse Dutch version of the website
        yield scrapy.Request(
            url=self.start_url % {"lang": "NL", "year": self.year},
            callback=self.parse,
            meta={"lang": "nl"},
        )

    def parse(self, response, **kwargs):
        """Parse list of program chunks."""
        # Get a list with program ids (chunks)
        chunks = response.css("div.notLoaded::attr(data)").extract()
        # For each chunk, request the programs.
        for chunk in chunks:
            yield response.follow(
                self.search_url % {"lang": response.meta["lang"], "chunks": chunk},
                self.parse_programs,
                meta=response.meta,
            )

    def parse_programs(self, response, **kwargs):
        """Parse a single chunk of programs."""
        # Get all URLs of programs
        program_urls = response.css("a.clickableDiv::attr(href)").extract()
        # For every program url, parse the program.
        for program_url in program_urls:
            yield response.follow(program_url, self.parse_program, meta=response.meta)

    def parse_program(self, response, **kwargs):
        # Get the program code
        program_code = response.css("h1#titleLabel::attr(data-code)").extract_first()
        # Get the faculty code from the logo url
        faculty_code = response.css(".logoFaculteit::attr(style)").re_first(
            r"url\(.*/([A-Z]{2}).*\)$"
        )
        # Get the faculty name from the text after the marker
        faculty = FacultyItem(
            name=response.css(".glyphicon-map-marker")
            .xpath("../div")
            .css("span::text")
            .extract_first(),
            id_code=program_code[0],
            code=f"F{faculty_code}",
        )
        # Compose program
        program_name = response.css("h1#titleLabel::text").extract_first()
        program_education_type = (
            response.css("div.headerItem").css("span::text").extract_first()
            if response.css("div.headerItem").css("span::text").extract_first()
            else ""
        )
        program = ProgramItem(
            code=program_code,
            name=program_name,
            education_type=program_education_type,
            faculty_item=faculty,
        )
        # Fetch the courses for the program
        program_course_url = (
            response.url.rsplit("/", 1)[0] + "/programma/" + response.url.split("/")[-1]
        )
        yield response.follow(
            program_course_url,
            self.parse_course_offerings,
            meta={"program": program} | response.meta,
        )

    # noinspection PyMethodMayBeStatic
    def parse_course_offerings(self, response, **kwargs):
        """Parse course offerings"""
        # Get all course offering rows
        rows = response.css("th.cursusnaam").xpath("../../../../table/tbody/tr")

        # For each table parse the course offerings
        for row in rows:
            code = row.css("td.cursusnaam span::attr(title)").extract_first()
            name = row.css("td.cursusnaam span::text").extract_first()
            languages = (
                row.css("td.taal span::text").extract_first()
                if row.css("td.taal span::text").extract_first()
                else ""
            )

            # If the course does not have a name or a code, it is probably an empty row.
            # In this case, skip it.
            if name is None or code is None:
                continue

            instructor_code = row.css("td.lesgever span::text").extract_first()
            if instructor_code:
                instructor_code = instructor_code.split("/")[-1]

            instructor = EmployeeItem(
                code=instructor_code,
                name=row.css("td.lesgever span::text").extract_first(),
            )

            # Compose course offering
            course = CourseItem(
                code=code,
                name=name,
                languages=languages,
                instructor_item=instructor,
            )
            year = (
                row.css("td.mt1 span::text").extract_first()
                if row.css("td.mt1 span::text").extract_first()
                else ""
            )
            semester = (
                row.css("td.semester span::text").extract()
                if row.css("td.semester span::text").extract()
                else ""
            )
            credits = (
                float(row.css("td.SP span::text").extract_first().replace(",", "."))
                if row.css("td.SP span::text").extract_first()
                else None
            )
            # Yield course offering
            course_offering = CourseOfferingItem(
                course_item=course,
                year=year,
                semester=semester,
                credits=credits,
                program_item=response.meta["program"],
                lang=response.meta["lang"],
            )
            yield course_offering


__all__ = ["StudiekiezerSpider"]
