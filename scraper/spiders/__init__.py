# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

from .studiekiezer import StudiekiezerSpider

__all__ = ["StudiekiezerSpider"]
