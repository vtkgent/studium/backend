# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

from .useragents import UserAgentsMiddleware


__all__ = ["UserAgentsMiddleware"]
