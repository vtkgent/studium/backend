# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

import logging
from itertools import cycle
from typing import Iterator

import scrapy


logger = logging.getLogger("main")
DEFAULT_USER_AGENT = f"Scrapy/{scrapy.__version__} (+https://scrapy.org)"

class UserAgentsMiddleware:
    """Rotates User-Agent header."""

    next_user_agent: Iterator[str]

    @classmethod
    def from_crawler(cls, crawler):
        user_agents = crawler.settings.get("USER_AGENTS", [])
        if not user_agents:
            logger.warning("USER_AGENTS is not set, defaulting to %s.", DEFAULT_USER_AGENT)
            user_agents = [DEFAULT_USER_AGENT]
        return cls(user_agents)

    def __init__(self, user_agents):
        self.next_user_agent = cycle(user_agents)

    def process_request(self, request, spider):
        request.headers["User-Agent"] = next(self.next_user_agent)
