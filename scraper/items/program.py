# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

import scrapy
from scrapy_djangoitem import DjangoItem

from app.models import Program


class ProgramItem(DjangoItem):
    django_model = Program
    faculty_item = scrapy.Field()
    name = scrapy.Field()


__all__ = ["ProgramItem"]
