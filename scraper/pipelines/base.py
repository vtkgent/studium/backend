# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)


class BasePipeline:
    @staticmethod
    def create_or_update_from_item(cls, values=None, **kwargs):
        field_names = [field.name for field in cls._meta.get_fields()]
        values = {k: v for k, v in (values or {}).items() if k in field_names}
        values.update(kwargs)

        obj = None
        try:
            obj = cls.objects.get(**kwargs)
        except cls.DoesNotExist:
            obj = cls.objects.create(**kwargs)
            obj.update(**values)
        except:
            print("Exception occured.")
        else:
            obj.update(**values)
        return obj

