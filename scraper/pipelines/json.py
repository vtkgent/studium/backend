# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

import json
from datetime import datetime
from pathlib import Path

from itemadapter import ItemAdapter

from scraper.items import CourseOfferingItem


class JsonWriterPipeline:
    """Writes raw scraped data to a jl file. This data should be further processed and imported."""

    data: list

    def open_spider(self, spider):
        self.data = []

    def close_spider(self, spider):
        output = Path("output")
        output.mkdir(exist_ok=True)
        with open(
            output
            / f"{spider.name}_aj{spider.year}-{datetime.now().strftime('%Y%m%d_%H%M%S')}.jl",
            "w",
        ) as f:
            f.write("\n".join(json.dumps(item) for item in self.data))

    def process_item(self, item, spider):
        if isinstance(item, CourseOfferingItem):
            # Store course offerings
            self.data.append(ItemAdapter(item).asdict())
        # Pass item to next pipeline
        return item
