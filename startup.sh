#!/bin/bash
set -ex

# Check if there aren't any severe mistakes
python manage.py check --deploy

# Collect static files
python manage.py collectstatic --no-input

# Perform the migrations
python manage.py migrate

# Send mails periodically
while true; do python manage.py send_queued_mail; sleep 1m; done &

# Start the server
gunicorn \
    --bind=0.0.0.0:80 \
    --workers=3 \
    --worker-tmp-dir=/dev/shm \
    --timeout 300 \
    --log-file=- \
    --access-logfile=/var/log/access.log \
    --error-logfile=/var/log/error.log \
    --log-level debug \
    app.wsgi:application
