# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)
from django.contrib import admin
from django.db import models
from markdownx.widgets import AdminMarkdownxWidget
from reversion.admin import VersionAdmin

from app.models import Course, Document
from app.models import AdminGroup


class DocumentInline(admin.StackedInline):
    model = Document
    extra = 0


@admin.register(Course)
class CourseAdmin(VersionAdmin):
    filter_horizontal = ["programs", "subscriptions"]
    list_display = ("code", "name_en", "name_nl", "instructor")
    search_fields = ("code", "name_en", "name_nl")
    list_filter = ("programs",)
    inlines = [DocumentInline]
    formfield_overrides = {models.TextField: {"widget": AdminMarkdownxWidget}}

    def get_queryset(self, request):
        customQS = AdminGroup.objects.none()
        all_admin_groups = AdminGroup.objects.all()
        for a in all_admin_groups:
            if request.user in a.allowed_users.all():
                customQS = customQS | a.allowed_courses.all()
                for ap in a.allowed_programs.all():
                    customQS = customQS | ap.courses.all()
        if request.user.is_superuser:
            qs = super(CourseAdmin, self).get_queryset(request)
            return qs
        return customQS.distinct()
