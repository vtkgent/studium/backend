# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

from django import forms
from django.contrib import admin
from reversion.admin import VersionAdmin

from app.models import FlatPage


class FlatPageModelForm(forms.ModelForm):
    class Meta:
        model = FlatPage
        fields = [
            "name",
            "url",
            "description",
            "content",
        ]

        widgets = {
            "content": forms.Textarea(attrs={"class": "mceEditor", "rows": "50"}),
            "description": forms.Textarea(attrs={"class": "mceNoEditor"}),
        }

    class Media:
        js = [
            "/static/grappelli/tinymce/jscripts/tiny_mce/tiny_mce.js",
            "/static/grappelli/tinymce_setup/tinymce_setup.js",
        ]


@admin.register(FlatPage)
class FlatPageAdmin(VersionAdmin):
    form = FlatPageModelForm

    list_display = (
        "name",
        "url",
        "description",
    )
    search_fields = (
        "name",
        "url",
        "description",
    )
