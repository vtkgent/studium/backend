# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import gettext_lazy as _
from reversion.admin import VersionAdmin

from ..models import CourseOffering, Employee, Faculty, User
from .course import CourseAdmin  # noqa: F401
from .document import DocumentAdmin  # noqa: F401
from .flat_page import FlatPageAdmin  # noqa: F401
from .admin_group import AdminGroupAdmin  # noqa: F401
from .programs import ProgramAdmin  # noqa: F401

admin.site.register(Faculty, VersionAdmin)
admin.site.register(Employee, VersionAdmin)
admin.site.register(CourseOffering, VersionAdmin)


@admin.register(User)
class CustomUserAdmin(VersionAdmin, UserAdmin):
    fieldsets = (
        (None, {"fields": ("username", "password")}),
        (_("Personal info"), {"fields": ("first_name", "last_name", "email")}),
        (
            _("Permissions"),
            {
                "fields": (
                    "is_active",
                    "is_staff",
                    "is_superuser",
                    "groups",
                    "user_permissions",
                ),
            },
        ),
        (
            _("Important dates"),
            {"fields": ("last_login", "date_joined", "date_accepted_agreements")},
        ),
    )
    pass


__all__ = []
