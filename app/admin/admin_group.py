# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)
from django.contrib import admin
from django.db import models
from markdownx.widgets import AdminMarkdownxWidget
from reversion.admin import VersionAdmin

from app.models import AdminGroup


@admin.register(AdminGroup)
class AdminGroupAdmin(VersionAdmin):
    filter_horizontal = ["allowed_users", "allowed_courses", "allowed_programs"]
    list_display = ["name"]
    search_fields = ["name"]
    formfield_overrides = {models.TextField: {"widget": AdminMarkdownxWidget}}
