# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)
from django.contrib import admin
from django.db import models
from markdownx.widgets import AdminMarkdownxWidget
from reversion.admin import VersionAdmin

from app.models import Program
from app.models import AdminGroup


@admin.register(Program)
class ProgramAdmin(VersionAdmin):
    list_display = ("code", "name_nl", "name_en")
    search_fields = ("code", "name_nl", "name_en")
    list_filter = ("faculty", "education_type")
    formfield_overrides = {models.TextField: {"widget": AdminMarkdownxWidget}}

    def get_queryset(self, request):
        customQS = AdminGroup.objects.none()
        all_admin_groups = AdminGroup.objects.all()
        for a in all_admin_groups:
            if request.user in a.allowed_users.all():
                customQS = customQS | a.allowed_programs.all()
        if request.user.is_superuser:
            qs = super(ProgramAdmin, self).get_queryset(request)
            return qs
        return customQS.distinct()
