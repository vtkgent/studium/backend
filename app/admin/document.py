# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

from django.contrib import admin
from django.db import models
from markdownx.widgets import AdminMarkdownxWidget
from reversion.admin import VersionAdmin

from app.models import Document, DocumentTag, DocumentRating
from app.models import AdminGroup


@admin.register(Document)
class DocumentAdmin(VersionAdmin):
    filter_horizontal = ["tags"]
    list_display = ("name", "course", "author")
    search_fields = ("name", "course", "author")
    list_filter = ("course", "author")
    formfield_overrides = {models.TextField: {"widget": AdminMarkdownxWidget}}

    def get_queryset(self, request):
        customQS = AdminGroup.objects.none()
        all_admin_groups = AdminGroup.objects.all()
        for a in all_admin_groups:
            if request.user in a.allowed_users.all():
                for ac in a.allowed_courses.all():
                    customQS = customQS | ac.documents.all()
                for ap in a.allowed_programs.all():
                    for apc in ap.courses.all():
                        customQS = customQS | apc.documents.all()
        if request.user.is_superuser:
            qs = super(DocumentAdmin, self).get_queryset(request)
            return qs
        return customQS.distinct()


@admin.register(DocumentTag)
class DocumentTagAdmin(VersionAdmin):
    list_display = ["name"]
    search_fields = ["name"]
    formfield_overrides = {models.TextField: {"widget": AdminMarkdownxWidget}}


@admin.register(DocumentRating)
class DocumentRatingAdmin(VersionAdmin):
    pass
