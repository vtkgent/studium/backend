# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

from ratelimit import ALL
from functools import wraps
from ratelimit.exceptions import Ratelimited
from ratelimit.utils import is_ratelimited
from ratelimit.core import ip_mask


def unique_boolean(field, subset=None):
    """Allow to specify a unique boolean for a model."""

    def cls_factory(cls):
        def factory(func):
            @wraps(func)
            def decorator(self, *args, **kwargs):
                kwargs = {field: True}
                for arg in subset or []:
                    if getattr(self, arg):
                        kwargs[arg] = getattr(self, arg)
                instances = self.__class__.objects.filter(**kwargs)
                if getattr(self, field):
                    instances.exclude(pk=self.pk).update(**{field: False})
                elif instances.count() == 0:
                    setattr(self, field, True)
                return func(self)

            return decorator

        if hasattr(cls, "save"):
            cls.save = factory(cls.save)
        return cls

    return cls_factory


def GQLRatelimitKey(group, request):
    return request.gql_rl_field


def ratelimit(group=None, key=None, rate=None, method=ALL, block=False):
    def decorator(fn):
        @wraps(fn)
        def _wrapped(root, info, **kw):
            request = info.context

            old_limited = getattr(request, "limited", False)

            def callableKey(group, request):
                value = ip_mask(request.META["REMOTE_ADDR"])
                value2 = kw.get("doc", None)
                newValue = value + "_" + value2
                return newValue

            if not key:
                new_key = callableKey
            elif callable(key):
                new_key = key
            elif key.startswith("gql:"):
                _key = key.split("gql:")[1]
                value = kw.get(_key, None)
                if not value:
                    raise ValueError(f"Cannot get key: {key}")
                request.gql_rl_field = value

                new_key = GQLRatelimitKey
            else:
                new_key = key

            ratelimited = is_ratelimited(
                request=request,
                group=group,
                fn=fn,
                key=new_key,
                rate=rate,
                method=method,
                increment=True,
            )

            request.limited = ratelimited or old_limited

            if ratelimited and block:
                raise Ratelimited("You have exceeded the rate limit.")
            return fn(root, info, **kw)

        return _wrapped

    return decorator


__all__ = ["unique_boolean", "ratelimit"]
