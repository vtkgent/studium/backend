# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

import base64
import binascii


def convert_id(base64_id):
    try:
        conv_id = base64.b64decode(base64_id).decode("utf-8")
    except binascii.Error:
        return base64_id
    id_parts = conv_id.split(":")
    id = id_parts[1]
    return id
