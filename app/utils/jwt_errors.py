from graphql import GraphQLError
from graphql_jwt.refresh_token.utils import get_refresh_token_by_model


def custom_refresh_token(refresh_token_model, token, context=None):
    try:
        return get_refresh_token_by_model(
            refresh_token_model=refresh_token_model,
            token=token,
            context=context,
        )

    except refresh_token_model.DoesNotExist:
        return GraphQLError("Your session has expired. Please log in again.")
