# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

import re
from django.db.models import Q
from django.contrib.postgres.search import TrigramSimilarity


def code_name_search(queryset, name, value):
    return queryset.annotate(similarity=TrigramSimilarity("name_en", value)).filter(
        Q(code__istartswith=value) | Q(similarity__gte=0.2)
    )


def course_code_name_search(queryset, name, value):
    value = value.strip()
    coursecode_queryset = queryset.filter(
        Q(name_en__icontains=value)
        | Q(name_nl__icontains=value)
        | Q(instructor__name__icontains=value)
        | Q(code__istartswith=value)
    ).distinct()

    # An exact match with the course code is found
    if coursecode_queryset:
        queryset = coursecode_queryset

    # No exact match is found
    else:
        # Split string on certain characters and see if course contains all strings
        QRuntime = Q()
        QRuntime2 = Q()
        QRuntime3 = Q()
        values = re.compile("[a-zA-Z0-9]+").findall(value)
        values = list(
            filter(
                lambda x: x not in ["en", "de", "van", "in", "and", "of", "the"], values
            )
        )
        for val in values:
            QRuntime = QRuntime & Q(name_en__icontains=val)
        for val in values:
            QRuntime3 = QRuntime3 & Q(name_nl__icontains=val)
        for val in values:
            QRuntime2 = QRuntime2 & Q(instructor__name__icontains=val)
        QRuntime = QRuntime | QRuntime2 | QRuntime3
        coursecode_queryset_and = queryset.filter(QRuntime).distinct()
        if coursecode_queryset_and:
            queryset = coursecode_queryset_and
        else:
            # Split string and see if course contain any of the strings
            for val in values:
                QRuntime = QRuntime | Q(name_en__icontains=val)
                QRuntime = QRuntime | Q(name_nl__icontains=val)
                QRuntime = QRuntime | Q(instructor__name__icontains=val)
            coursecode_queryset_or = queryset.filter(QRuntime).distinct()
            if coursecode_queryset_or:
                queryset = coursecode_queryset_or
            else:
                # Compute TrigramSimilarity for the course names
                queryset = queryset.annotate(
                    similarity=TrigramSimilarity("name_en", value)
                ).distinct()
    return queryset


def human_code_name_search(queryset, name, value):
    return queryset.filter(Q(code__istartswith=value) | Q(name_en__icontains=value))
