from bleach.sanitizer import Cleaner

cleaner = Cleaner(
    tags=[
        "strong",
        "em",
        "s",
        "h1",
        "h2",
        "p",
        "br",
        "ol",
        "ul",
        "li",
        "th",
        "tr",
        "td",
        "pre",
        "code",
        "tbody",
        "table",
    ],
    attributes={
        "th": ["colspan", "rowspan"],
        "td": ["colspan", "rowspan"],
    },
    protocols=[],
    strip=True,
)


def clean_html(text):
    sanitized = cleaner.clean(text)
    return sanitized
