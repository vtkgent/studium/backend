# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

import string

import factory.fuzzy
from factory.django import DjangoModelFactory
from mdgen import MarkdownPostProvider

from app.models import Course

factory.Faker.add_provider(MarkdownPostProvider)


class CourseFactory(DjangoModelFactory):
    class Meta:
        model = Course
        django_get_or_create = ("code",)

    class Params:
        faculty_code = factory.fuzzy.FuzzyChoice(string.ascii_uppercase[:11])
        course_code = factory.fuzzy.FuzzyInteger(100000, 999999)

    code = factory.LazyAttribute(lambda c: f"{c.faculty_code}{c.course_code}")
