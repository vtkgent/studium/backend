# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

from app.factories.user import UserFactory
from app.factories.course import CourseFactory
import string

import factory.fuzzy
from factory.django import DjangoModelFactory
from mdgen import MarkdownPostProvider

from app.models import Document

factory.Faker.add_provider(MarkdownPostProvider)


class DocumentFactory(DjangoModelFactory):
    class Meta:
        model = Document
        django_get_or_create = ("name",)

    class Params:
        faculty_code = factory.fuzzy.FuzzyChoice(string.ascii_uppercase[:11])
        course_code = factory.fuzzy.FuzzyInteger(100000, 999999)

    course = factory.SubFactory(CourseFactory)
    author = factory.SubFactory(UserFactory)
    name = factory.Faker("file_name")
    id = factory.Sequence(lambda n: "{}".format(n))

    @factory.post_generation
    def tags(self, create, extracted, **kwargs):
        if not create:
            return

        if extracted:
            self.tags.add(extracted)
