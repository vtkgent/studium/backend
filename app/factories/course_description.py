# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

import string

import factory.fuzzy
from factory.django import DjangoModelFactory
from mdgen import MarkdownPostProvider

from app.models import CourseDescription

factory.Faker.add_provider(MarkdownPostProvider)


class CourseDescriptionFactory(DjangoModelFactory):
    class Meta:
        model = CourseDescription
        django_get_or_create = ("code",)

    class Params:
        faculty_code = factory.fuzzy.FuzzyChoice(string.ascii_uppercase[:11])
        course_code = factory.fuzzy.FuzzyInteger(100000, 999999)

    code = factory.LazyAttribute(lambda c: f"{c.faculty_code}{c.course_code}")
    name = factory.Faker("company")
    specifications_url = "https://studiegids.ugent.be"
