# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

import factory.fuzzy
from factory.django import DjangoModelFactory
from mdgen import MarkdownPostProvider

from app.models import DocumentTag

factory.Faker.add_provider(MarkdownPostProvider)


class DocumentTagFactory(DjangoModelFactory):
    class Meta:
        model = DocumentTag
        django_get_or_create = ("name",)

    name = factory.Sequence(lambda n: "Tag{}".format(n))
