# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

from app.factories.document import DocumentFactory
import factory.fuzzy
from factory.django import DjangoModelFactory
from mdgen import MarkdownPostProvider

from app.models import DocumentRating
from app.factories.user import UserFactory

factory.Faker.add_provider(MarkdownPostProvider)


class DocumentRatingFactory(DjangoModelFactory):
    class Meta:
        model = DocumentRating

    user = factory.SubFactory(UserFactory)
    document = factory.SubFactory(DocumentFactory)
    rating = factory.fuzzy.FuzzyDecimal(5)


class UserWithRatingFactory(UserFactory):
    membership = factory.RelatedFactory(
        DocumentRatingFactory,
        factory_related_name="user",
    )


class UserWith2RatingsFactory(UserFactory):
    membership1 = factory.RelatedFactory(
        DocumentRatingFactory,
        factory_related_name="user",
        document__name="Doc1",
    )
    membership2 = factory.RelatedFactory(
        DocumentRatingFactory,
        factory_related_name="user",
        document__name="Doc2",
    )
