# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

import logging
from logging.config import dictConfig

from django.core.management.base import BaseCommand
from scrapy.crawler import CrawlerProcess
from scrapy.utils.project import get_project_settings

from scraper.spiders import StudiekiezerSpider

logger = logging.getLogger("main")


class Command(BaseCommand):
    help = "Scrape everything into a yaml file"

    def handle(self, *_args, **_kwargs):
        # Create scrapy process
        process = CrawlerProcess(get_project_settings(), install_root_handler=False)

        # Reconfigure logging (only print info to stdout/stderr)
        logging_config = {
            "version": 1,
            "disable_existing_loggers": True,
            "formatters": {"standard": {"format": "[%(levelname)s] %(message)s"}},
            "handlers": {
                "console": {
                    "class": "logging.StreamHandler",
                    "formatter": "standard",
                    "level": logging.INFO,
                    "stream": "ext://sys.stderr",
                },
                "file": {
                    "class": "logging.FileHandler",
                    "formatter": "standard",
                    "level": logging.DEBUG,
                    "filename": "scrapy.log",
                },
            },
            "loggers": {
                "main": {"handlers": ["console"]},
                "hpack": {"handlers": ["console", "file"], "level": logging.ERROR},
                "scrapy": {"handlers": ["console", "file"]},
                "twisted": {
                    "handlers": ["console", "file"],
                    "level": logging.ERROR,
                },
            },
        }
        dictConfig(logging_config)

        # Start crawlers
        logger.info("Starting crawlers.")
        process.crawl(StudiekiezerSpider)
        process.start()
