# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)
import logging
import sys
from itertools import chain
from pathlib import Path

from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.management import call_command
from django.core.management.base import BaseCommand

logger = logging.getLogger("main")


User = get_user_model()


class Command(BaseCommand):
    help = "Initialize the database with data."

    def handle(self, *args, **options):
        if not settings.DEBUG:
            logger.error("Trying to initialize data in a production environment")
            sys.exit(1)

        logger.info("Migrating database ...")
        call_command("migrate")

        logger.info("Importing fixtures ...")
        for d in settings.FIXTURE_DIRS:
            d = Path(d)
            fixtures = chain(d.glob("*.json"), d.glob("*.yaml"))
            for f in sorted(fixtures):
                call_command("loaddata", f)

        logger.info("Creating superuser (studium:studium) ...")
        if not User.objects.filter(username="studium"):
            User.objects.create_superuser("studium", "studium@vtk.ugent.be", "studium")
