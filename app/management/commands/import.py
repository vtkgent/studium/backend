# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

import json
import logging
from collections import defaultdict
from decimal import Decimal
from pathlib import Path
from time import perf_counter

from django.core import serializers
from django.core.management import call_command
from django.core.management.base import BaseCommand
from django.db import transaction

from app.models import Course, CourseOffering, Employee, Faculty, Program

logger = logging.getLogger("main")


class Command(BaseCommand):
    help = "Imports a given jl file and updates the data."

    store: dict

    def add_arguments(self, parser):
        parser.add_argument(
            "-f", "--file", dest="path", type=str, help="jl file", required=True
        )
        parser.add_argument(
            "-c", "--code", dest="code", type=str, help="faculty code filter"
        )

    def handle(self, *_args, **options):
        path = Path(options["path"])
        if not path.exists():
            logger.error("file %s does not exist", path)
            return

        # Migrate database
        call_command("migrate")

        # Load data from path
        logger.info("Loading data from file.")
        start = perf_counter()
        with open(path) as f:
            data = [json.loads(line) for line in f]
        # Filter faculty
        if code := options.get("code", None):
            data = [
                e
                for e in data
                if e.get("program_item", {}).get("faculty_item", {}).get("id_code", "")
                == code
            ]
        logger.info("Loaded data in %.2f seconds.", perf_counter() - start)

        if not data:
            logger.error("file does not contain data")
            return

        # Get new objects from data
        logger.info("Generating objects from data.")
        start = perf_counter()
        self.generate_objects(data)
        logger.info("Generated objects in %.2f seconds.", perf_counter() - start)

        # Generate diffset
        logger.info("Generating diffset from objects.")
        start = perf_counter()
        self.generate_diffsets()
        logger.info("Generated diffset in %.2f seconds.", perf_counter() - start)

        # Print diffset stats
        max_len = max(len(obj_class.__name__) for obj_class in self.diffsets)
        for obj_class, diffset in self.diffsets.items():
            a, m, d = [len(diffset.get(k, [])) for k in ("add", "mod", "del")]
            logger.info(
                "%s   \u001b[32;1m%s \u001b[34;1m%s \u001b[31;1m%s\u001b[0m",
                obj_class.__name__.ljust(max_len),
                f"+{a}".rjust(6),
                f"~{m}".rjust(6),
                f"-{d}".rjust(6),
            )

        diff_path = path.with_suffix(".diff")
        with open(diff_path, "w") as f:
            for obj_class, diffset in self.diffsets.items():
                obj_fields = [
                    field.name
                    for field in obj_class._meta.fields
                    if not field.primary_key and not field.many_to_many
                ]
                for prefix, key in [("+", "add"), ("~", "mod"), ("-", "del")]:
                    for obj in diffset[key]:
                        serialized = serializers.serialize(
                            "json", [obj], fields=obj_fields
                        )[1:-2]
                        f.write(f"{prefix} {serialized}\n")
        logger.info("Check %s to verify details.", diff_path)

        # Show confirmation (defaults to No)
        try:
            while (reply := input("Proceed with import? [y/N] ").lower()) not in "yn":
                pass
        except KeyboardInterrupt:
            return
        if reply != "y":
            return

        # Apply changes
        start = perf_counter()
        ordered = [
            (k, self.diffsets.get(k, []))
            for k in (Faculty, Program, Employee, Course, CourseOffering)
        ]
        with transaction.atomic():
            for obj_class, diffset in ordered:
                logger.info("Updating %s.", obj_class.__name__)
                obj_fields = [
                    f.name for f in obj_class._meta.fields if not f.primary_key
                ]
                # Create added objects (update primary keys for related fields)
                added = obj_class.objects.bulk_create(diffset["add"])
                for (old, new) in zip(diffset["add"], added):
                    old.pk = new.pk
                # Update modified objects
                obj_class.objects.bulk_update(diffset["mod"], fields=obj_fields)
                # TODO: make del items inactive
        logger.info("Updated everything in %.2f seconds.", perf_counter() - start)

    def generate_objects(self, data: list[dict]):
        self.store = defaultdict(dict)
        for item in data:
            # Unwind item
            program_item = item["program_item"]
            faculty_item = program_item["faculty_item"]
            course_item = item["course_item"]
            instructor_item = course_item["instructor_item"]
            lang = item["lang"]

            # Update faculty
            faculty = self.update_object(
                Faculty,
                "id_code",
                id_code=faculty_item["id_code"],
                code=faculty_item["code"],
                **{f"name_{lang}": faculty_item["name"]},
            )

            # Update program
            program = self.update_object(
                Program,
                "code",
                code=program_item["code"],
                **{f"name_{lang}": program_item["name"]},
                education_type=program_item["education_type"],
                faculty=faculty,
            )

            # Update instructor
            instructor = (
                self.update_object(
                    Employee,
                    "code",
                    code=instructor_item["code"],
                    name=instructor_item["name"],
                )
                if instructor_item["code"]
                else None
            )

            # Update course
            course = self.update_object(
                Course,
                "code",
                code=course_item["code"],
                **{f"name_{lang}": course_item["name"]},
                languages=course_item["languages"],
                instructor=instructor,
            )

            # Update offerings
            for semester in item["semester"]:
                self.update_object(
                    CourseOffering,
                    "program__code",
                    "course__code",
                    program=program,
                    course=course,
                    semester=semester[-1] if "sem" in semester else "J",
                    year=item["year"],
                    credits=Decimal(f"{item['credits']:2.1f}"),
                )

    def update_object(self, obj_class, *id_fields, **values):
        # Cleanup strings
        values = {k: v.strip() if isinstance(v, str) else v for k, v in values.items()}
        # Get identifying values
        id_fields = tuple(id_fields)
        id_values = tuple(self.unwrapprop(values, k) for k in id_fields)
        # Get from storage, else get from database, else create new
        item = (
            self.store.get(obj_class, {}).get(id_values, None)
            or obj_class.objects.filter(**dict(zip(id_fields, id_values))).first()
            or obj_class()
        )
        # Update values with new data
        for k, v in values.items():
            if not v:
                continue
            if isinstance(v, str):
                # Join multiple languages
                # TODO: Move this to the scraper? We do not want old language data
                # TODO: This is fine for now, and still better than selecting at random
                if k == "languages":
                    langs = getattr(item, k, "").split(",") + v.split(",")
                    langs = {lang.strip() for lang in langs if lang.strip()}
                    v = ",".join(sorted(langs))
            # Set attribute
            setattr(item, k, v)
        # Store item
        self.store[obj_class][id_values] = item
        return item

    @classmethod
    def unwrapprop(cls, obj, prop):
        def getprop(obj, prop):
            return obj[prop] if isinstance(obj, dict) else getattr(obj, prop)

        if "__" in prop:
            prop1, prop2 = prop.split("__", 1)
            return cls.unwrapprop(getprop(obj, prop1), prop2)
        return getprop(obj, prop)

    def generate_diffsets(self):
        def is_equal_field(a, b):
            a__code, b__code = getattr(a, "code", None), getattr(b, "code", None)
            if a__code and b__code:
                return a__code == b__code
            return a == b

        def is_equal(a, b):
            return all(is_equal_field(getattr(a, k), getattr(b, k)) for k in obj_fields)

        self.diffsets = defaultdict(dict)
        for obj_class, obj_dict in self.store.items():
            obj_fields = [
                field.name for field in obj_class._meta.fields if not field.primary_key
            ]
            obj_old = {obj.pk: obj for obj in obj_class.objects.all()}
            obj_new = obj_dict.values()
            diffset = defaultdict(list)
            for obj in obj_new:
                pk = getattr(obj, "pk", None)
                if not pk:
                    # New object
                    diffset["add"].append(obj)
                elif pk and pk in obj_old.keys():
                    # Object already exists, only add if modified
                    if not is_equal(obj, obj_old[pk]):
                        diffset["mod"].append(obj)
                else:
                    # Object shouldn't exist anymore
                    diffset["del"].append(obj)
            self.diffsets[obj_class] = diffset
