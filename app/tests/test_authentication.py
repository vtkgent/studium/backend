# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

import os
from glob import glob
import inspect
import importlib
import graphene

from app.tests.base import BaseTestCase


class AuthenticationTestCase(BaseTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.ALL_UNAUTH_QUERIES = ["FlatPageQuery"]
        cls.ALL_UNAUTH_MUTATIONS = [
            "ObtainJWTMixin",
            "CASObtainJSONWebToken",
            "Refresh",
            "Revoke",
            "AcceptAgreements",
        ]

    # Test if all required queries require login
    def test_authentication_queries(self):
        folderPath = "/app/app/schema/queries/[!studium_]*.py"
        for file in glob(folderPath):
            name = os.path.splitext(os.path.basename(file))[0]
            if name != "__init__":
                module = importlib.import_module("app.schema.queries.{0}".format(name))
                clsmembers = inspect.getmembers(module, inspect.isclass)
                for member in clsmembers:
                    if member[0] not in self.ALL_UNAUTH_QUERIES and isinstance(
                        member[1],
                        graphene.utils.subclass_with_meta.SubclassWithMeta_Meta,
                    ):
                        fields = inspect.getmembers(member[1])
                        for field in fields:
                            if field[0] == "__init_subclass_with_meta__" and not field[
                                1
                            ].__func__.__qualname__.startswith(
                                ("DjangoObjectType", "Scalar", member[0])
                            ):
                                self.assertTrue(
                                    field[1].__func__.__qualname__.startswith(
                                        "LoginRequiredObjectType"
                                    )
                                )

    # Test if all required mutations require login
    def test_authentication_mutations(self):
        folderPath = "/app/app/schema/mutations/*.py"
        for file in glob(folderPath):
            name = os.path.splitext(os.path.basename(file))[0]
            if name != "__init__":
                module = importlib.import_module(
                    "app.schema.mutations.{0}".format(name)
                )
                clsmembers = inspect.getmembers(module, inspect.isclass)
                for member in clsmembers:
                    if member[0] not in self.ALL_UNAUTH_MUTATIONS:
                        fields = inspect.getmembers(member[1])
                        for field in fields:
                            if field[0] == "__init_subclass_with_meta__" and not field[
                                1
                            ].__func__.__qualname__.startswith(
                                ("ObjectType", "DjangoObjectType", "Scalar", member[0])
                            ):
                                print(field[1].__func__.__qualname__)
                                self.assertTrue(
                                    field[1].__func__.__qualname__.startswith(
                                        "LoggedinUserMutationType"
                                    )
                                )
