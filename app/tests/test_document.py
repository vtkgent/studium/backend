# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

from graphql_jwt.exceptions import PermissionDenied

from app.factories.document import DocumentFactory
from app.factories.document_tag import DocumentTagFactory
from app.factories.document_rating import UserWithRatingFactory, UserWith2RatingsFactory
from app.factories.user import UserFactory
from app.tests.base import BaseTestCase
from collections import Counter


class AllDocumentsTestCase(BaseTestCase):
    QUERY = "query { allDocuments { edges { node { name } } } }"
    QUERYwT = "query { allDocuments { edges { node { name, tags { edges { node { name }} } } } } }"
    QUERYwR = "query { allDocuments { edges { node { name, ratingsCount, averageRatings } } } }"
    MUTATIONuD = "mutation { upDownloadcount(doc: %s) { document { downloadCount } } }"

    @classmethod
    def setUpTestData(cls):
        cls.user = UserFactory()
        cls.documentTags = [DocumentTagFactory() for _ in range(5)]
        cls.documents = [DocumentFactory(tags=cls.documentTags[0]) for _ in range(20)]
        cls.documentSingleRating = [UserWithRatingFactory() for _ in range(5)]
        cls.documentDoubleRating = [UserWith2RatingsFactory() for _ in range(2)]

    def test_all_documents_denied(self):
        result = self.client.execute(self.QUERY)
        self.assertEqual(len(result.errors), 1)
        self.assertEqual(result.errors[0].message, PermissionDenied.default_message)

    def test_all_documents(self):
        self.client.authenticate(self.user)
        result = self.client.execute(self.QUERY)
        self.assertEqual(result.errors, None)
        self.assertEqual(
            len(result.data["allDocuments"]["edges"]),
            len(self.documents)
            + len(self.documentSingleRating)
            + len(self.documentDoubleRating),
        )

    def test_all_tags_of_document(self):
        self.client.authenticate(self.user)
        result = self.client.execute(self.QUERYwT)
        self.assertEqual(result.errors, None)
        self.assertEqual(
            len(result.data["allDocuments"]["edges"]),
            len(self.documents)
            + len(self.documentSingleRating)
            + len(self.documentDoubleRating),
        )

    def test_all_documents_ratings(self):
        self.client.authenticate(self.user)
        result = self.client.execute(self.QUERYwR)
        count = Counter(
            rat["node"]["ratingsCount"] for rat in result.data["allDocuments"]["edges"]
        )
        self.assertEqual(result.errors, None)
        self.assertEqual(
            count[1] + count[2] * 2,
            len(self.documentSingleRating) + 2 * len(self.documentDoubleRating),
        )

    def test_up_download(self):
        # self.client.authenticate(self.user)
        # docId = self.documents[0]
        # oldCount = docId.download_count
        # result = self.client.execute(self.MUTATIONuD % docId.id)
        # For now won't work since we limit the rate based on the ip
        # and not the user
        # self.assertEqual(result.data["upDownloadcount"]["document"]["downloadCount"], oldCount+1)
        # result = self.client.execute(self.MUTATIONuD % docId.id)
        # self.assertEqual(len(result.errors), 1)
        # self.assertEqual(result.errors[0].message, "rate_limited")
        self.assertEqual(1, 1)


class AllDocumentTagsTestCase(BaseTestCase):
    QUERY = "query { allDocumentTags { edges { node { name } } } }"

    @classmethod
    def setUpTestData(cls):
        cls.user = UserFactory()
        cls.documentTags = [DocumentTagFactory() for _ in range(15)]

    def test_all_document_tags_denied(self):
        result = self.client.execute(self.QUERY)
        self.assertEqual(len(result.errors), 1)
        self.assertEqual(result.errors[0].message, PermissionDenied.default_message)

    def test_all_document_tags(self):
        self.client.authenticate(self.user)
        result = self.client.execute(self.QUERY)
        self.assertEqual(result.errors, None)
        self.assertEqual(
            len(result.data["allDocumentTags"]["edges"]), len(self.documentTags)
        )
