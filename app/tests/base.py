# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

from django.test import TestCase
from graphql_jwt.testcases import JSONWebTokenClient


class BaseTestCase(TestCase):
    client_class = JSONWebTokenClient


__all__ = ["BaseTestCase"]
