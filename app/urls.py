# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

import django_cas_ng.views
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.http import HttpResponse
from django.urls import include, path, re_path
from django.views.generic import RedirectView
from filebrowser.sites import site as filebrowser_site

from app.schema.views import SentryGraphQLView
from app.views import SAMLValidateMock

urlpatterns = [
    # Admin
    path("admin/filebrowser/", filebrowser_site.urls),
    path("admin/", admin.site.urls, name="admin"),
    path("grappelli/", include("grappelli.urls"), name="grappelli"),
    re_path(r"^markdownx/", include("markdownx.urls")),
    # Project
    path("graphql/", SentryGraphQLView.as_view(), name="graphql"),
    path("healthcheck/", lambda _: HttpResponse(), name="healthcheck"),
    # CAS
    path(
        "accounts/login", django_cas_ng.views.LoginView.as_view(), name="cas_ng_login"
    ),
    path(
        "accounts/logout",
        django_cas_ng.views.LogoutView.as_view(),
        name="cas_ng_logout",
    ),
]

if settings.DEBUG:
    urlpatterns += [
        path(
            "samlValidate",
            SAMLValidateMock.as_view(),
            name="saml-validate",
        ),
    ]
if settings.DEBUG:
    urlpatterns += [path("", RedirectView.as_view(url="graphql"), name="index")]
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
else:
    urlpatterns += [
        path("sentry-debug/", lambda _: 1 / 0, name="sentry-debug"),
    ]
