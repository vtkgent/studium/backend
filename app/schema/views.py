from django.conf import settings
from django.http import HttpRequest
from django.views.decorators.csrf import csrf_exempt
from graphene_file_upload.django import FileUploadGraphQLView
from sentry_sdk import start_transaction


class SentryGraphQLView(FileUploadGraphQLView):
    """Custom view for clean error reporting in Sentry.

    Inspired by:
    https://medium.com/open-graphql/monitoring-graphene-django-python-graphql-api-using-sentry-c0b0c07a344f
    """

    @classmethod
    def as_view(cls, *args, **kwargs):
        # Using csrf_exempt here is safe because JWT is immune to CSRF attacks
        return csrf_exempt(super().as_view(*args, graphiql=settings.DEBUG, **kwargs))

    def execute_graphql_request(
        self,
        request: HttpRequest,
        data,
        query,
        variables,
        operation_name,
        show_graphiql=False,
    ):
        operation_type = (
            self.get_backend(request)
            .document_from_string(self.schema, query)
            .get_operation_type(operation_name)
        )
        with start_transaction(op=operation_type, name=operation_name):
            return super().execute_graphql_request(
                request,
                data,
                query,
                variables,
                operation_name,
                show_graphiql,
            )
