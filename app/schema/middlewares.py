from graphql import ResolveInfo
from sentry_sdk import capture_exception
from sentry_sdk.integrations.logging import ignore_logger

# Ignore default Graphene tracebacks
ignore_logger("graphql.execution.utils")


class SentryMiddleware:
    """Custom middleware for clean error reporting in Sentry."""

    def on_error(self, error):
        capture_exception(error)
        raise error

    def resolve(self, next, root, info: ResolveInfo, **args):
        return next(root, info, **args).catch(self.on_error)
