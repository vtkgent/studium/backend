from app.schema.mutations.base import LoggedinUserMutationType
import graphene
from post_office import mail

from app.utils.error_handling import studium_clean
from graphql import GraphQLError


class ContactSendMutation(LoggedinUserMutationType):
    """Mutation that will send an email with the given contact form information."""

    class Arguments:
        subject = graphene.String(required=True)
        message = graphene.String(required=True)

    ok = graphene.Boolean()

    @classmethod
    def clean_subject(cls, subject):
        # Check if the subject exists
        if not subject:
            return GraphQLError("Subject is required")

        # Check if the subject length is within the limits
        if len(subject) < 4:
            return GraphQLError("Subject must be at least 4 characters")
        if len(subject) > 250:
            return GraphQLError("Subject cannot be larger than 250 characters")

    @classmethod
    def clean_message(cls, message):
        # Check if the message exists
        if not message:
            return GraphQLError("Message is required")

        # Check if the message length is within the limits
        if len(message) < 10:
            return GraphQLError("Message must be at least 10 characters")
        if len(message) > 1000:
            return GraphQLError("Message cannot be larger than 1000 characters")

    @classmethod
    def mutate(cls, parent, info, subject: str, message: str):

        # Checks every clean-function and returns corresponding errors
        studium_clean(ContactSendMutation, **locals())

        # Get the current user
        user = info.context.user

        if not user:
            return GraphQLError("User could not be identified.")

        name = user.first_name + " " + user.last_name
        email = user.email

        if not name or name == "":
            return GraphQLError("Name of user could not be found.")

        if not email or email == "":
            return GraphQLError("Email of user could not be found.")

        # Send email
        mail.send(
            recipients="contact@studium.gent",
            sender="contact@studium.gent",
            cc=email,
            headers={"Reply-To": email},
            template="contact",
            context={
                "name": name,
                "email": email,
                "subject": subject,
                "message": message,
            },
        )

        return ContactSendMutation(ok=True)


class ContactMutation(graphene.ObjectType):
    send_contact = ContactSendMutation.Field()
