# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

import datetime

import graphene
from graphene import Boolean


class AcceptAgreements(graphene.Mutation):
    accepted = Boolean()

    @classmethod
    def mutate(cls, parent, info):
        # Get the current user
        user = info.context.user

        # Update the last agreement date
        user.date_accepted_agreements = datetime.datetime.now()
        user.save()

        return AcceptAgreements(accepted=True)


class AgreementsMutation(graphene.ObjectType):
    accept_agreements = AcceptAgreements.Field()
