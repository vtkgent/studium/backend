# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

from urllib.parse import urljoin, urlparse

import graphene
import graphql_jwt
from cas import CASClientWithSAMLV1
from django.conf import settings
from django.utils.translation import gettext_lazy as _
from graphene.utils.thenables import maybe_thenable
from graphql import GraphQLError
from graphql_jwt import exceptions, signals
from graphql_jwt.decorators import (
    csrf_rotation,
    on_token_auth_resolve,
    refresh_expiration,
    setup_jwt_cookie,
)
from graphql_jwt.mixins import ObtainJSONWebTokenMixin as ObtainJWTMixin
from graphql_jwt.mixins import ResolveMixin
from graphql_jwt.refresh_token.decorators import ensure_refresh_token
from graphql_jwt.shortcuts import get_refresh_token
from sentry_sdk import capture_exception, set_context

from app.models import User


class CASObtainJSONWebToken(ObtainJWTMixin, ResolveMixin, graphene.Mutation):
    """Mutation that obtains a JSON web token for subsequent requests.

    This mutation validates a CAS ticket granted by UGent and authenticated the
    user if it is valid. The user it's CAS attributes will be updated. When the
    user does not exist a new user is created.
    """

    class Arguments:
        ticket = graphene.String()
        service = graphene.String(required=False)

    @classmethod
    @setup_jwt_cookie
    @csrf_rotation
    @refresh_expiration
    def mutate(cls, root, info, **kwargs):
        """Verify the CAS ticket and resolve the query if it is valid."""
        context = info.context
        context._jwt_token_auth = True

        if not (service_url := kwargs.get("service")):
            # Try to fallback to the HTTP Referer
            if not (referer := info.context.META.get("HTTP_REFERER", None)):
                # Validation must happen with the exact same url as the request was made
                # with. If this isn't possible, abort.
                raise exceptions.JSONWebTokenError(
                    _("Something went wrong validating your credentials"),
                )
            # Construct the service from the referer, but without any arguments
            service_url = urljoin(referer, urlparse(referer).path)

        # Add extra information for debugging
        client_kwargs = {
            "service_url": service_url,
            "server_url": settings.CAS_SERVER_URL,
        }
        set_context("cas-client", client_kwargs)
        # Create the CAS client
        cas = CASClientWithSAMLV1(**client_kwargs)

        # Validate the ticket against the server
        ticket = kwargs.pop("ticket")
        username, attributes, _pgtiou = cas.verify_ticket(ticket)
        if not username:
            e = exceptions.JSONWebTokenError(
                _("Please enter valid credentials"),
            )
            capture_exception(e)
            raise e

        # Check if the user is a student
        object_class = attributes.get("objectClass", set())
        valid_str = isinstance(object_class, str) and object_class == "ugentStudent"
        valid_list = isinstance(object_class, list) and "ugentStudent" in object_class
        if not valid_str and not valid_list:
            raise exceptions.PermissionDenied(
                _("You don't have access to this platform"),
            )

        # Create user if user does not exist
        if not (user := User.objects.filter(username=username).first()):
            user = User.objects.create_user(
                username=username,
                email=attributes.get("mail", ""),
            )

        # Add extra information for debugging
        info_context = {"username": username, "attributes": attributes}
        set_context("cas-info", info_context)

        # Update email
        user.email = attributes.get("mail", None) or user.email or ""

        # Update name
        user.first_name = attributes.get("givenname", None) or user.first_name or ""
        user.last_name = attributes.get("surname", None) or user.last_name or ""

        # Update UGent info
        user.university_id = (
            attributes.get("ugentStudentID", None) or user.university_id or ""
        )
        user.faculty = attributes.get("faculty", None) or user.faculty or ""
        user.last_enrolled = int(
            next(
                iter(attributes.get("lastenrolled", "").split("-")),
                0,
            ),
        )

        # Save user model
        user.save()

        if hasattr(context, "user"):
            context.user = user

        result = cls.resolve(root, info, **kwargs)
        signals.token_issued.send(sender=cls, request=context, user=user)
        return maybe_thenable((context, user, result), on_token_auth_resolve)


class Refresh(graphql_jwt.Refresh):
    """Mutation that refreshes a token."""

    @classmethod
    @ensure_refresh_token
    def refresh(cls, root, info, refresh_token, **kwargs):
        context = info.context
        old_refresh_token = get_refresh_token(refresh_token, context)
        # This check is missing in the original implementation of the Refresh mutation
        # causing an obscure error for the end user when a token is expired
        if isinstance(old_refresh_token, GraphQLError):
            return old_refresh_token
        # Call original method
        return super().refresh(root, info, refresh_token=refresh_token, **kwargs)


class Revoke(graphql_jwt.Revoke):
    """Mutation that revokes a refresh token."""

    @classmethod
    @ensure_refresh_token
    def revoke(cls, root, info, refresh_token, **kwargs):
        context = info.context
        refresh_token_obj = get_refresh_token(refresh_token, context)
        # This check is missing in the original implementation of the Revoke mutation
        # causing an obscure error for the end user when a token is expired
        if isinstance(refresh_token_obj, GraphQLError):
            return refresh_token_obj
        # Call original method
        return super().revoke(root, info, refresh_token=refresh_token, **kwargs)


class AuthMutation(graphene.ObjectType):
    cas_token_auth = CASObtainJSONWebToken.Field()
    verify_token = graphql_jwt.Verify.Field()
    refresh_token = Refresh.Field()
    revoke_token = Revoke.Field()
