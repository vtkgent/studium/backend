# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

import pathlib
import re

import graphene
import reversion
import zipfile
import magic
import os
import shutil
import datetime
from django.core.files.uploadedfile import InMemoryUploadedFile
from django.utils import timezone
from graphene_file_upload.scalars import Upload
from graphql import GraphQLError
from PyPDF2 import PdfFileReader
from post_office import mail

from app.models import Course, Document, DocumentTag
from app.schema.queries import CourseType
from app.schema.queries.document import DocumentType
from app.utils.error_handling import studium_clean
from django.conf import settings
from app.schema.mutations.base import LoggedinUserMutationType
from app.utils.id_converter import convert_id
from app.utils.decorators import ratelimit


class DocumentUploadMutation(LoggedinUserMutationType):
    class Arguments:
        course_code = graphene.String(required=True)
        name = graphene.String(required=True)
        file = Upload(required=True)
        tags = graphene.List(graphene.String)
        anonymous = graphene.Boolean(required=False)

    document = graphene.Field(DocumentType)

    @classmethod
    def clean_course_code(cls, course_code):
        # Check if the course_code belongs to an existing course
        if not Course.objects.filter(code=course_code).first():
            return GraphQLError("This course doesn't exist!")

    @classmethod
    def clean_name(cls, name):
        # Check if the name exists
        if not name:
            return GraphQLError("Name is a required field. ")

        # Check if the name length is within the limits
        if len(name) < 6:
            return GraphQLError("Subject must be at least 6 characters")
        if len(name) > 50:
            return GraphQLError("Name cannot be larger than 50 characters")

    @classmethod
    def clean_file(cls, file):
        # Check if a file was uploaded
        if not file or not file.file:
            return GraphQLError("It is required to upload a file. ")

        # Check if filetype is valid
        allowed_type = False
        for extension in Document.document_extensions:
            # Dangerous! These are executables
            # Only allow .mws (maple files)
            if file.content_type == "application/octet-stream":
                parts = file.name.split(".")
                extens = parts[-1]
                for py_extension in Document.py_document_extensions:
                    if extens == py_extension:
                        allowed_type = True
            if re.match(extension, file.content_type):
                allowed_type = True

        if not allowed_type:
            return GraphQLError("This is not a valid filetype.")

        if file.size > settings.MAX_FILE_SIZE:
            return GraphQLError("File is too big.")

        if file.content_type == "application/zip":
            myzip = zipfile.ZipFile(file, "r")
            timee = int(datetime.datetime.now().timestamp() * 1000000)
            path = "tempUpload" + str(timee)
            os.mkdir(path)
            myzip.extractall(path)
            for temppath, _, tempfiles in os.walk(path):
                for name in tempfiles:
                    allowed_type = False
                    f = pathlib.PurePath(temppath, name)
                    file_extension = magic.from_file(str(f), mime=True)
                    for extension in Document.document_extensions:
                        if re.match("application/zip", file_extension):
                            shutil.rmtree(path)
                            return GraphQLError(
                                "ZIP file is not allowed to contain another ZIP file."
                            )
                        if re.match(extension, file_extension):
                            allowed_type = True
                        if file_extension == "application/octet-stream":
                            parts = name.split(".")
                            extens = parts[-1]
                            for py_extension in Document.py_document_extensions:
                                if extens == py_extension:
                                    allowed_type = True
                    if not allowed_type:
                        shutil.rmtree(path)
                        return GraphQLError(
                            "ZIP file contains an unsupported file type."
                        )
            shutil.rmtree(path)
        # TODO: check if file already exists? (hash)

    @classmethod
    def clean_tags(cls, tags):
        # Check if all uploaded tags are valid
        if len(tags) == 0:
            return GraphQLError("No tags were given.")
        if tags[0] == "":
            return GraphQLError("No tags were given.")
        for tag in tags:
            if not DocumentTag.objects.filter(name=tag):
                return GraphQLError("This tag does not exist.")

    @classmethod
    def clean_course_code_name(cls, variables):
        if Document.objects.filter(
            name=variables["name"],
            course=Course.objects.filter(code=variables["course_code"]).first(),
        ).exists():
            return GraphQLError("This document name already exists.")

    @classmethod
    def clean_document_path(cls, variables):
        if (
            not Course.objects.filter(code=variables["course_code"])
            .first()
            .programs.first()
            or not Course.objects.filter(code=variables["course_code"])
            .first()
            .programs.first()
            .faculty
        ):
            return GraphQLError("This course isn't currently offered at any faculty. ")

        if (
            not Course.objects.filter(code=variables["course_code"])
            .first()
            .programs.first()
            .faculty.code
        ):
            return GraphQLError(
                "The information of the faculty associated with this course could "
                "not be retrieved. "
            )

    @classmethod
    def mutate(
        cls,
        parent,
        info,
        course_code: str,
        name: str,
        file: InMemoryUploadedFile,
        tags: str,
        anonymous: bool,
        **kwargs,
    ):
        # Checks every clean-function and returns corresponding errors
        studium_clean(cls, **locals())

        # Get the course
        course = Course.objects.filter(code=course_code).first()

        if not course:
            return GraphQLError("Course could not be found.")

        # Get the current user
        user = info.context.user

        if not user:
            return GraphQLError("User could not be found")

        # Change file name
        extension = file.name.split(".").pop()
        file.name = name + "." + extension

        # Create a new document and make a first revision
        with reversion.create_revision():
            document = Document.objects.create(
                course=course,
                author=user,
                name=name,
                file=file,
                # TODO: Calculate or specify? PDF's calculated, other files usefull?
                page_count=0,
                anonymous=anonymous,
            )
            for tag in tags:
                document.tags.add(DocumentTag.objects.filter(name=tag).first())

            # Load the pdf to the PdfFileReader object with default settings
            if file.content_type == "application/pdf":
                with file.open("rb") as pdf_file:
                    pdf_reader = PdfFileReader(pdf_file)
                    document.page_count = pdf_reader.numPages
                document.save()

            # Add meta-information.
            reversion.set_user(user)
            # TODO: Sensible comment
            reversion.set_comment("Created revision 1")
        return DocumentUploadMutation(document=document)


class DocumentEditRatingMutation(LoggedinUserMutationType):
    class Arguments:
        id = graphene.ID()
        rating = graphene.Int(required=True)

    document = graphene.Field(DocumentType)

    @classmethod
    def mutate(
        cls,
        parent,
        info,
        id,
        rating,
    ):
        # Convert base64 id to integer
        id = convert_id(id)

        # Get the course
        document = Document.objects.filter(pk=id).first()

        if not document:
            return GraphQLError("Document could not be found.")

        # Get the current user
        user = info.context.user

        if not user:
            return GraphQLError("User could not be found.")

        # Create a new document and make a first revision
        with reversion.create_revision():
            documentrating = document.document_ratings.filter(user_id=user.id).first()

            if documentrating:
                documentrating.rating = rating
                documentrating.save()
            else:
                document.ratings.add(user, through_defaults={"rating": rating})
                document.save()

            # Add meta-information.
            reversion.set_user(user)
            # TODO: Sensible comment
            now = timezone.now()
            reversion.set_comment(
                f"By {info.context.user.name} on {now.date()} "
                f"at {now.time().isoformat('seconds')}"
            )
        return DocumentEditRatingMutation(document=document)


class DocumentUpDownloadMutation(LoggedinUserMutationType):
    class Arguments:
        doc = graphene.ID()
        url = graphene.String()

    document = graphene.Field(DocumentType)

    @staticmethod
    def validateUrl(url, id):
        # Currently we assume all files will be in the media map
        # If that changes in the future or the files will be stored
        # remotely, use something like httplib to access the url,
        # get the headers and see if the status equals 200.
        if not os.path.isfile("media/" + url):
            raise GraphQLError(
                (
                    "File with url: {}, has been deleted."
                    "Yet a database object with id: {} still exists."
                ).format(url, id)
            )

    @ratelimit(rate="1/h", block=True)
    def mutate(
        cls,
        parent,
        doc,
        **kwargs,
    ):
        # Check if url is given as argument
        url = kwargs.get("url", None)

        if url:
            DocumentUpDownloadMutation.validateUrl(url, doc)

        # Convert base64 id to integer
        id = convert_id(doc)

        # Get the course
        document = Document.objects.filter(pk=id).first()

        if not document:
            return GraphQLError("Document is not found.")

        document.download_count += 1
        document.save()

        return DocumentUpDownloadMutation(document=document)


class DocumentReportMutation(LoggedinUserMutationType):
    """Mutation that will send an email with a report of a document."""

    class Arguments:
        code = graphene.String(required=True)
        filename = graphene.String(required=True)
        message = graphene.String(required=True)

    ok = graphene.Boolean()
    course = graphene.Field(CourseType)

    @classmethod
    def clean_message(cls, message):
        # Check if the message exists
        if not message:
            return GraphQLError("Message is required")

        # Check if the message length is within the limits
        if len(message) < 10:
            return GraphQLError("Message must be at least 10 characters")
        if len(message) > 1000:
            return GraphQLError("Message cannot be larger than 1000 characters")

    @classmethod
    def mutate(cls, parent, info, code: str, filename: str, message: str):

        # Checks every clean-function and returns corresponding errors
        studium_clean(cls, **locals())

        # Make sure the course exists
        if not Course.objects.filter(code=code).first():
            return GraphQLError("Course does not exist.")

        # Get the current user
        if not (user := info.context.user):
            return GraphQLError("User could not be identified.")

        name = user.first_name + " " + user.last_name
        email = user.email

        if not name or name == "":
            return GraphQLError("Name of user could not be found.")

        if not email or email == "":
            return GraphQLError("Email of user could not be found.")

        # Send email
        mail.send(
            recipients="contact@studium.gent",
            sender="contact@studium.gent",
            cc=email,
            headers={"Reply-To": email},
            template="report",
            context={
                "name": name,
                "email": email,
                "subject": f"{code} - {filename}",
                "message": message,
            },
        )
        return cls(ok=True)


class DocumentDeleteMutation(LoggedinUserMutationType):
    class Arguments:
        id = graphene.ID()

    document = graphene.Field(DocumentType)

    @classmethod
    def mutate(
        cls,
        parent,
        info,
        id,
    ):
        # Convert base64 id to integer
        id = convert_id(id)

        # Get the course
        document = Document.objects.filter(pk=id).first()

        if not document:
            return GraphQLError("The document with id %s does not exist. " % id)

        # Get the current user
        user = info.context.user

        if not user:
            return GraphQLError("User could not be identified.")

        if user.id == document.author.id:
            document.delete()
        else:
            return GraphQLError("You are not authorised to delete this file. ")

        if Document.objects.filter(pk=id).first():
            return GraphQLError("The document could not be deleted.")

        return DocumentDeleteMutation(document=document)


class DocumentMutation(graphene.ObjectType):
    upload_document = DocumentUploadMutation.Field()
    update_rating = DocumentEditRatingMutation.Field()
    up_downloadcount = DocumentUpDownloadMutation.Field()
    report_document = DocumentReportMutation.Field()
    delete_document = DocumentDeleteMutation.Field()
