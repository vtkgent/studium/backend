# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

import graphene
from graphql_jwt.decorators import login_required


class LoggedinUserMutationType(graphene.Mutation, abstract=True):
    @classmethod
    def __init_subclass_with_meta__(cls, **kwargs):
        # Decorate all mutate's with login_required
        # Must be done before super since we edit a method
        # and super initialises it
        for attr in cls.__dict__:
            if attr == "mutate":
                mut = attr
        setattr(cls, mut, login_required(getattr(cls, mut)))
        super(LoggedinUserMutationType, cls).__init_subclass_with_meta__(**kwargs)


__all__ = ["LoggedinUserMutationType"]
