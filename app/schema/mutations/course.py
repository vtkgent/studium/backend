# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

from app.schema.mutations.base import LoggedinUserMutationType
import graphene

from django.utils import timezone
from graphene import Boolean, Field, String
from graphql import GraphQLError
from reversion import add_meta, create_revision, set_comment, set_user
from reversion.models import Version

from app.models import Course, CourseDescriptionVersion
from app.schema.queries import CourseType
from app.utils.id_converter import convert_id
from app.utils.sanitizer import clean_html


class CourseUpdateDescriptionMutation(LoggedinUserMutationType):
    class Arguments:
        code = graphene.String(required=True)
        content = graphene.String(required=True)

    course = Field(CourseType)

    @classmethod
    def mutate(cls, parent, info, code, content):
        course = Course.objects.filter(code=code).first()

        # Make sure the course exists
        if not course:
            return GraphQLError("Course does not exist.")

        # Get the description
        description = course.description

        # Sanitize the content
        content = clean_html(content)

        # Get the previous description version
        previous_version = CourseDescriptionVersion.objects.filter(
            description=description
        ).last()

        with create_revision():
            description.content = content

            # Save the changes
            description.save()

            # Set the revision metadata
            set_user(info.context.user)
            now = timezone.now()
            set_comment(
                f"By {info.context.user.name} on {now.date()} "
                f"at {now.time().isoformat('seconds')}"
            )

            # Create the CourseDescriptionVersion
            add_meta(
                CourseDescriptionVersion,
                description=description,
                number=Version.objects.get_for_object(description).count() + 1,
                reverted_version=None,
                previous_version=previous_version,
            )

        return CourseUpdateDescriptionMutation(course=course)


class CourseRevertDescriptionMutation(LoggedinUserMutationType):
    class Arguments:
        revert_version_id = graphene.ID()
        code = graphene.String()

    course = Field(CourseType)

    @classmethod
    def mutate(cls, parent, info, revert_version_id, code):
        # Convert base64 id to integer
        revert_version_id = convert_id(revert_version_id)

        course = Course.objects.filter(code=code).first()
        reverted_version = CourseDescriptionVersion.objects.filter(
            id=revert_version_id
        ).first()

        # Make sure the course exists
        if not course:
            return GraphQLError("Course does not exist.")

        # Make sure the description revert version exists
        if not reverted_version:
            return GraphQLError("Description revert version does not exist.")

        # Get the description
        description = course.description

        # Get the last description version
        last_version = CourseDescriptionVersion.objects.last()

        # Create a new revision
        with create_revision():
            # Revert to the given reverted version
            reverted_version.revision.revert()

            # Set the revision metadata
            set_user(info.context.user)
            now = timezone.now()
            set_comment(
                f"By {info.context.user.name} on {now.date()} "
                f"at {now.time().isoformat('seconds')}"
            )

            # Create the CourseDescriptionVersion
            add_meta(
                CourseDescriptionVersion,
                description=description,
                number=Version.objects.get_for_object(description).count() + 1,
                reverted_version=reverted_version,
                previous_version=last_version,
            )

            # Refresh the course description in the database to fetch
            # the latest description
            course.description.refresh_from_db()

        return CourseRevertDescriptionMutation(course=course)


class CourseUpdateSubscriptionMutation(LoggedinUserMutationType):
    class Arguments:
        value = Boolean(required=True)
        code = String()

    course = Field(CourseType)

    @classmethod
    def mutate(cls, parent, info, value, code):
        user = getattr(info.context, "user", None)
        course = Course.objects.filter(code=code).first()

        # Make sure the course exists
        if not course:
            return GraphQLError("Course does not exist.")

        if not user:
            return GraphQLError("User cannot be identified")

        with create_revision():
            if value:
                course.subscriptions.add(user)
            else:
                course.subscriptions.remove(user)

            course.save()
            set_user(user)
            now = timezone.now()
            set_comment(
                f"{user.name} subscribed to course {code} "
                f"on {now.date()} at {now.time().isoformat('seconds')}"
            )
        return CourseUpdateSubscriptionMutation(course=course)


class CourseMutation(graphene.ObjectType):
    update_course_description = CourseUpdateDescriptionMutation.Field()
    revert_course_description = CourseRevertDescriptionMutation.Field()
    update_course_subscription = CourseUpdateSubscriptionMutation.Field()


__all__ = ["CourseMutation"]
