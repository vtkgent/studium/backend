# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

from graphene import relay
from graphene_django.filter import DjangoFilterConnectionField
from graphene_django.types import DjangoObjectType

from app.models import Employee

from .base import LoginRequiredObjectType


class EmployeeType(DjangoObjectType):
    class Meta:
        model = Employee
        fields = ["code", "name"]
        filter_fields = ["name"]
        interfaces = [relay.Node]


class EmployeeQuery(LoginRequiredObjectType):
    employee = relay.Node.Field(EmployeeType)
    all_employees = DjangoFilterConnectionField(EmployeeType)


__all__ = ["EmployeeType", "EmployeeQuery"]
