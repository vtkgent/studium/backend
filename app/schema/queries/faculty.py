# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

import django_filters
import graphene
from graphene import relay
from graphene_django.filter import DjangoFilterConnectionField
from graphene_django.types import DjangoObjectType

from app.models import Course, Faculty
from app.utils.search import human_code_name_search

from .base import LoginRequiredObjectType
from .course import CourseType


class FacultyFilter(django_filters.FilterSet):
    search = django_filters.CharFilter(method=human_code_name_search)
    order_by = django_filters.OrderingFilter(fields={"code": "code", "name_en": "name"})

    class Meta:
        model = Faculty
        fields = ["search", "order_by"]


class FacultyType(DjangoObjectType):
    code = graphene.String(source="code")
    courses = DjangoFilterConnectionField(CourseType)
    name = graphene.Field(graphene.String)

    @staticmethod
    def resolve_courses(parent, info, **kwargs):
        return Course.objects.filter(offerings__program__faculty__pk=parent.pk)

    @staticmethod
    def resolve_name(parent, info, **kwargs):
        if parent.name_nl != "":
            return parent.name_nl
        return parent.name_en

    class Meta:
        model = Faculty
        fields = ["name_en", "name_nl", "programs"]
        filterset_class = FacultyFilter
        interfaces = [relay.Node]


class FacultyQuery(LoginRequiredObjectType):
    faculty = relay.Node.Field(FacultyType)
    all_faculties = DjangoFilterConnectionField(FacultyType)


__all__ = ["FacultyType", "FacultyQuery"]
