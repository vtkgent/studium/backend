# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

import django_filters
import graphene
from graphene_django.types import DjangoObjectType

from app.models import CourseDescription, CourseDescriptionVersion


class CourseDescriptionFilter(django_filters.FilterSet):
    class Meta:
        model = CourseDescription
        fields = []


class CourseDescriptionType(DjangoObjectType):
    versions = graphene.List(
        "app.schema.queries.course_description_version.CourseDescriptionVersionType"
    )
    last_version_user = graphene.Field("app.schema.queries.user.UserType")
    last_version_date = graphene.Field(graphene.String)
    versions_amount = graphene.Field(graphene.Int)

    @staticmethod
    def resolve_versions(description, info, **kwargs):
        return (
            CourseDescriptionVersion.objects.filter(description=description)
            .all()
            .order_by("-id")
        )

    @staticmethod
    def resolve_last_version_date(description, info, **kwargs):
        # Get the last description version
        last_description_version = (
            CourseDescriptionVersion.objects.filter(description=description)
            .order_by("-id")
            .first()
        )

        # If no version yet, return null
        if not last_description_version:
            return None

        return last_description_version.revision.date_created

    @staticmethod
    def resolve_last_version_user(description, info, **kwargs):
        # Get the last description version
        last_description_version = (
            CourseDescriptionVersion.objects.filter(description=description)
            .order_by("-id")
            .first()
        )

        # If no version yet, return null
        if not last_description_version:
            return None

        return last_description_version.revision.user

    @staticmethod
    def resolve_versions_amount(description, info, **kwargs):
        return CourseDescriptionVersion.objects.filter(description=description).count()

    class Meta:
        model = CourseDescription
        fields = ["content"]
        filterset_class = CourseDescriptionFilter
        interfaces = [graphene.relay.Node]


__all__ = ["CourseDescriptionType"]
