# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

import json
import re
from collections import Counter

import diff_match_patch as dmp_module
import django_filters
import graphene
from graphene_django.types import DjangoObjectType
from graphql.error.base import GraphQLError
from reversion.models import Version

from app.models import CourseDescriptionVersion

from .base import LoginRequiredObjectType


class CourseDescriptionVersionFilter(django_filters.FilterSet):
    class Meta:
        model = CourseDescriptionVersion
        fields = []


class CourseDescriptionVersionType(DjangoObjectType):
    content = graphene.Field(graphene.String)
    changes = graphene.Field(graphene.String)
    changes_added_amount = graphene.Field(graphene.Int)
    changes_deleted_amount = graphene.Field(graphene.Int)

    @staticmethod
    def resolve_content(version, info, **kwargs):
        # Get the internal Django Reversion versions
        version_internal = (
            Version.objects.get_for_object(version.description)
            .filter(id=version.revision.id)
            .first()
        )

        if not version_internal:
            return GraphQLError("Version could not be found.")

        # Get the version content
        content = json.loads(version_internal.serialized_data)

        if not content:
            return GraphQLError("No content was found for this version.")

        content = content[0]["fields"]["content"]

        return content

    @staticmethod
    def resolve_changes(version, info, **kwargs):

        # Get the diff
        dmp, diff = CourseDescriptionVersionType.get_diff(version, info, **kwargs)

        # Convert the diffs to HTML
        diff_html = dmp.diff_prettyHtml(diff)

        return diff_html

    @staticmethod
    def resolve_changes_added_amount(version, info, **kwargs):
        # Get the diff
        dmp, diff = CourseDescriptionVersionType.get_diff(version, info, **kwargs)

        if not diff:
            return 0

        # Get the count
        count = Counter(elem[0] for elem in diff)

        return count[1]

    @staticmethod
    def resolve_changes_deleted_amount(version, info, **kwargs):
        # Get the diff
        dmp, diff = CourseDescriptionVersionType.get_diff(version, info, **kwargs)

        if not diff:
            return 0

        # Get the count
        count = Counter(elem[0] for elem in diff)

        return count[-1]

    @staticmethod
    def get_diff(given_version, info, **kwargs):
        """Get the diff between a given version and the previous version."""
        dmp = dmp_module.diff_match_patch()

        if not dmp:
            return GraphQLError("Difference between versions could not be caculated.")

        # Delete the HTML from a given string
        def clear_html(html):
            clear_regex = re.compile("<.*?>|&([a-z0-9]+|#[0-9]{1,6}|#x[0-9a-f]{1,6});")
            return re.sub(clear_regex, "", html)

        # Get the previous version
        previous_version = given_version.previous_version

        given_version_internal = (
            Version.objects.get_for_object(given_version.description)
            .filter(revision__id=given_version.revision.id)
            .first()
        )

        if not given_version_internal:
            return GraphQLError("Current version could not be found.")

        # Get the current versions content
        given_content = json.loads(given_version_internal.serialized_data)
        given_content = given_content[0]["fields"]["content"]
        given_content = clear_html(given_content)

        # Make sure the previous version exists
        # If not, return difference between current and empty string
        if not previous_version:
            diff = dmp.diff_main("", given_content)
            dmp.diff_cleanupSemantic(diff)
            return dmp, diff

        # Get the internal Django Reversion versions
        previous_version_internal = (
            Version.objects.get_for_object(previous_version.description)
            .filter(revision__id=previous_version.revision.id)
            .first()
        )

        # Get the previous versions content
        previous_content = json.loads(previous_version_internal.serialized_data)
        previous_content = previous_content[0]["fields"]["content"]
        previous_content = clear_html(previous_content)

        # Calculate the diff
        diff = dmp.diff_main(previous_content, given_content)

        # Cleanup the diff
        dmp.diff_cleanupSemantic(diff)

        return dmp, diff

    @staticmethod
    class Meta:
        model = CourseDescriptionVersion
        fields = [
            "revision",
            "number",
            "previous_version",
            "reverted_version",
            "changes_added_amount",
            "changes_deleted_amount",
        ]
        filterset_class = CourseDescriptionVersionFilter
        interfaces = [graphene.relay.Node]


class CourseDescriptionVersionQuery(LoginRequiredObjectType):
    course_description_version_by_id = graphene.relay.Node.Field(
        CourseDescriptionVersionType
    )


__all__ = ["CourseDescriptionVersionType", "CourseDescriptionVersionQuery"]
