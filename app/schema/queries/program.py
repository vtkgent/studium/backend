# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

import django_filters
from graphene import relay
import graphene
from graphene_django.filter import DjangoFilterConnectionField
from graphene_django.types import DjangoObjectType

from app.models import Course, Program
from app.utils.search import code_name_search

from .base import LoginRequiredObjectType
from .course import CourseType


class ProgramFilter(django_filters.FilterSet):
    search = django_filters.CharFilter(method=code_name_search)
    order_by = django_filters.OrderingFilter(fields={"code": "code", "name_en": "name"})

    # Fields for searching programs
    faculty = django_filters.CharFilter(field_name="faculty__code", distinct=True)

    class Meta:
        model = Program
        fields = ["search", "order_by", "faculty"]


class ProgramType(DjangoObjectType):
    courses = DjangoFilterConnectionField(CourseType)
    name = graphene.Field(graphene.String)

    @staticmethod
    def resolve_courses(parent, info, **kwargs):
        return Course.objects.filter(offerings__program__pk=parent.pk)

    @staticmethod
    def resolve_name(parent, info, **kwargs):
        if parent.name_nl != "":
            return parent.name_nl
        return parent.name_en

    class Meta:
        model = Program
        fields = ["code", "name_en", "name_nl", "faculty"]
        filterset_class = ProgramFilter
        interfaces = [relay.Node]


class ProgramQuery(LoginRequiredObjectType):
    program = relay.Node.Field(ProgramType)
    all_programs = DjangoFilterConnectionField(ProgramType)


__all__ = ["ProgramType", "ProgramQuery"]
