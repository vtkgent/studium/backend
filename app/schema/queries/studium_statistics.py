# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

import graphene
import datetime
from django.utils import timezone
from reversion.models import Version

from app.models import Document, User, CourseDescription, AdminGroup

from .base import LoginRequiredObjectType


class StatisticsType(graphene.ObjectType):
    new_documents_count = graphene.Int()
    new_users_count = graphene.Int()
    downloads_count = graphene.Int()
    descriptions_count = graphene.Int()
    period_number = graphene.Int()
    year = graphene.Int()


class TotalStatisticsType(graphene.ObjectType):
    total_documents_count = graphene.Int()
    total_users_count = graphene.Int()


class ComparisonStatisticsType(graphene.ObjectType):
    student_society = graphene.String()
    documents_uploaded = graphene.Int()


class StatisticsQuery(LoginRequiredObjectType):
    weekly_statistics = graphene.List(
        StatisticsType,
        number=graphene.Int(required=True),
        faculty=graphene.String(required=True),
    )
    monthly_statistics = graphene.List(
        StatisticsType,
        number=graphene.Int(required=True),
        faculty=graphene.String(required=True),
    )
    total_statistics = graphene.Field(
        TotalStatisticsType, faculty=graphene.String(required=True)
    )

    comparison_statistics = graphene.List(
        ComparisonStatisticsType,
        year=graphene.Int(required=True),
    )

    @staticmethod
    def resolve_weekly_statistics(parent, info, number, faculty):
        current_date = timezone.now()
        curr_year, curr_week, curr_day = current_date.isocalendar()
        stats_list = []
        for w in range(curr_week, curr_week - number, -1):
            stats = {}
            year = curr_year
            if w < 1:
                w = w % 52
                year -= 1
            stats["period_number"] = w
            stats["year"] = year
            if faculty == "all":
                docs = Document.objects.filter(
                    upload_date__week=w, upload_date__year=year
                )
            else:
                docs = Document.objects.filter(
                    upload_date__week=w,
                    upload_date__year=year,
                    course__offerings__program__faculty__code=faculty,
                ).distinct()
            stats["new_documents_count"] = docs.count()
            users = User.objects.filter(date_joined__week=w, date_joined__year=year)
            stats["new_users_count"] = users.count()
            stats_list.append(stats)
            downloads = (
                Version.objects.get_for_model(Document)
                .filter(
                    revision__date_created__week=w, revision__date_created__year=year
                )
                .exclude(revision__comment="Created revision 1")
            )
            stats["downloads_count"] = downloads.count()
            descriptions = Version.objects.get_for_model(CourseDescription).filter(
                revision__date_created__week=w, revision__date_created__year=year
            )
            stats["descriptions_count"] = descriptions.count()
        return stats_list

    def resolve_monthly_statistics(parent, info, number, faculty):
        current_date = timezone.now()
        curr_year, curr_week, curr_day = current_date.isocalendar()
        curr_month = datetime.datetime.now().month
        stats_list = []
        for m in range(curr_month, curr_month - number, -1):
            stats = {}
            year = curr_year
            if m < 1:
                m = m + 12
                year -= 1
            stats["period_number"] = m
            stats["year"] = year
            if faculty == "all":
                docs = Document.objects.filter(
                    upload_date__month=m, upload_date__year=year
                )
            else:
                docs = Document.objects.filter(
                    upload_date__month=m,
                    upload_date__year=year,
                    course__offerings__program__faculty__code=faculty,
                ).distinct()
            stats["new_documents_count"] = docs.count()
            users = User.objects.filter(date_joined__month=m, date_joined__year=year)
            stats["new_users_count"] = users.count()
            stats_list.append(stats)
            downloads = (
                Version.objects.get_for_model(Document)
                .filter(
                    revision__date_created__month=m, revision__date_created__year=year
                )
                .exclude(revision__comment="Created revision 1")
            )
            stats["downloads_count"] = downloads.count()
            descriptions = Version.objects.get_for_model(CourseDescription).filter(
                revision__date_created__month=m, revision__date_created__year=year
            )
            stats["descriptions_count"] = descriptions.count()
        return stats_list

    def resolve_total_statistics(parent, info, faculty, **kwargs):
        stats = {}
        if faculty == "all":
            stats["total_documents_count"] = Document.objects.count()
        else:
            stats["total_documents_count"] = (
                Document.objects.filter(
                    course__offerings__program__faculty__code=faculty
                )
                .distinct()
                .count()
            )
        stats["total_users_count"] = User.objects.count()
        return stats

    def resolve_comparison_statistics(parent, info, year):
        stats = []
        admin_groups = AdminGroup.objects.all()
        for a in admin_groups:
            stat = {}
            stat["student_society"] = a.name
            allowed_programs = a.allowed_programs.all()
            all_docs = Document.objects.none()
            for ap in allowed_programs:
                docs = Document.objects.filter(
                    upload_date__year__gte=year,
                    course__offerings__program__code=ap.code,
                ).distinct()
                all_docs = all_docs | docs
            allowed_courses = a.allowed_courses.all()
            for ac in allowed_courses:
                docs = Document.objects.filter(
                    upload_date__year__gte=year,
                    course__code=ac.code,
                ).distinct()
                all_docs = all_docs | docs
            stat["documents_uploaded"] = len(all_docs)
            stats.append(stat)
        stats = sorted(stats, key=lambda d: d["documents_uploaded"], reverse=True)
        return stats


__all__ = ["StatisticsQuery"]
