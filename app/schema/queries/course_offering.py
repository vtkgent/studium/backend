# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

import django_filters
import graphene
from graphene import relay
from graphene_django.filter import DjangoFilterConnectionField
from graphene_django.types import DjangoObjectType
from graphql_jwt.decorators import login_required

from app.models import Course, CourseOffering
from app.utils.search import course_code_name_search

from .base import LoginRequiredObjectType


class CourseOfferingFilter(django_filters.FilterSet):
    search = django_filters.CharFilter(method=course_code_name_search)
    order_by = django_filters.OrderingFilter(fields=["name_en"])

    class Meta:
        model = Course
        fields = ["search", "order_by"]

    def filter_queryset(self, queryset):
        queryset = super().filter_queryset(queryset)
        if hasattr(queryset.first(), "similarity"):
            queryset = queryset.order_by("-similarity")
        return queryset


class CourseOfferingType(DjangoObjectType):
    class Meta:
        model = CourseOffering
        fields = [
            "program",
            "course",
            "semester",
            "year",
            "credits",
        ]
        filter_fields = {}
        interfaces = [relay.Node]


class CourseOfferingQuery(LoginRequiredObjectType):
    course_offering = relay.Node.Field(CourseOfferingType)
    all_course_offerings = DjangoFilterConnectionField(CourseOfferingType)
    all_years = graphene.List(graphene.String, program=graphene.String(required=False))
    all_semesters = graphene.List(
        graphene.String, program=graphene.String(required=False)
    )

    @staticmethod
    @login_required
    def resolve_all_years(parent, info, **kwargs):
        course_offerings = CourseOffering.objects.filter(year__isnull=False).exclude(
            year=""
        )
        if "program" in kwargs:
            course_offerings = course_offerings.filter(program__code=kwargs["program"])
        return sorted(set(course_offerings.values_list("year", flat=True)))

    @staticmethod
    @login_required
    def resolve_all_semesters(parent, info, **kwargs):
        course_offerings = CourseOffering.objects.filter(semester__isnull=False)
        if "program" in kwargs:
            course_offerings = course_offerings.filter(program__code=kwargs["program"])
        return sorted(set(course_offerings.values_list("semester", flat=True)))


__all__ = ["CourseOfferingType", "CourseOfferingQuery"]
