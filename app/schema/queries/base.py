# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

import graphene
from graphene import String, relay
from graphene.types.typemap import TypeMap
from graphene_django import DjangoObjectType
from graphql_jwt.decorators import login_required
from reversion.models import Revision, Version


class LoginRequiredObjectType(graphene.ObjectType):
    @classmethod
    def __init_subclass_with_meta__(
        cls,
        interfaces=(),
        possible_types=(),
        default_resolver=None,
        _meta=None,
        **options,
    ):
        super().__init_subclass_with_meta__(
            interfaces=interfaces,
            possible_types=possible_types,
            default_resolver=default_resolver,
            _meta=_meta,
            **options,
        )
        if cls.__name__ != "Query":
            # Wrap every resolver with the login_required decorator
            for name, field in cls._meta.fields.items():
                resolver = TypeMap.get_resolver_for_type(
                    None, cls, name, field.default_value
                )
                setattr(cls, f"resolve_{name}", login_required(resolver))


class RevisionType(DjangoObjectType):
    class Meta:
        model = Revision
        fields = ["date_created", "user"]
        filter_fields = []
        interfaces = (relay.Node,)

    comment = String()

    @staticmethod
    def resolve_comment(root, info, **kwargs):
        return root.get_comment()


class VersionType(DjangoObjectType):
    class Meta:
        model = Version
        fields = [
            "content_type",
            "object_id",
            "format",
            "serialized_data",
            "revision",
        ]
        filter_fields = []
        interfaces = (relay.Node,)


__all__ = ["LoginRequiredObjectType"]
