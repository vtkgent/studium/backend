# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

from graphene import ObjectType

from .agreements import AcceptedAgreementsQuery
from .course import CourseQuery, CourseType
from .course_description import CourseDescriptionType
from .course_description_version import (
    CourseDescriptionVersionQuery,
    CourseDescriptionVersionType,
)
from .course_offering import CourseOfferingQuery, CourseOfferingType
from .document import DocumentExtensionsQuery, DocumentQuery
from .document_rating import DocumentRatingQuery
from .document_tag import DocumentTagQuery
from .employee import EmployeeQuery, EmployeeType
from .faculty import FacultyQuery, FacultyType
from .flat_page import FlatPageQuery, FlatPageType
from .program import ProgramQuery, ProgramType
from .studium_statistics import StatisticsQuery, StatisticsType, TotalStatisticsType
from .user import UserQuery, UserType


class Query(
    AcceptedAgreementsQuery,
    CourseQuery,
    CourseDescriptionVersionQuery,
    CourseOfferingQuery,
    DocumentExtensionsQuery,
    DocumentRatingQuery,
    DocumentTagQuery,
    DocumentQuery,
    EmployeeQuery,
    FacultyQuery,
    FlatPageQuery,
    ProgramQuery,
    StatisticsQuery,
    UserQuery,
    ObjectType,
):
    pass


__all__ = [
    "CourseType",
    "CourseOfferingType",
    "CourseDescriptionType",
    "CourseDescriptionVersionType",
    "EmployeeType",
    "FacultyType",
    "FlatPageType",
    "ProgramType",
    "Query",
    "StatisticsType",
    "TotalStatisticsType",
    "UserType",
]
