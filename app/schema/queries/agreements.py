# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

import graphene

from app.models import User as UserModel

from .base import LoginRequiredObjectType


class AcceptedAgreementsQuery(LoginRequiredObjectType):
    accepted_agreement = graphene.Boolean(url=graphene.String(required=True))

    @staticmethod
    def resolve_accepted_agreement(parent, info, url, **kwargs):
        """Get if the user has accepted an agreement on a specific flatpage url."""
        me = getattr(info.context, "user", None)

        return me.has_accepted_agreement(url)

    class Meta:
        model = UserModel
        fields = []


__all__ = ["AcceptedAgreementsQuery"]
