# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

import graphene
from graphene_django.types import DjangoObjectType
from graphql import GraphQLError
from reversion.models import Version

from app.models import FlatPage


class FlatPageType(DjangoObjectType):
    name = graphene.Field(graphene.String)
    content = graphene.Field(graphene.String)
    last_updated_date = graphene.Field(graphene.DateTime)

    @staticmethod
    def resolve_last_updated_date(parent, info, **kwargs):
        version = (
            Version.objects.get_for_object(parent)
            .order_by("-revision__date_created")
            .first()
        )

        # Make sure the version exists
        if not version:
            return None

        return version.revision.date_created

    class Meta:
        model = FlatPage
        fields = ["name", "content"]
        interfaces = [graphene.relay.Node]


class FlatPageQuery(graphene.ObjectType):
    flat_page = graphene.Field(FlatPageType, url=graphene.String(required=True))

    @staticmethod
    def resolve_flat_page(parent, info, url):
        flat_page = FlatPage.objects.filter(url=url).first()

        # Make sure the flatpage exists
        if not flat_page:
            return GraphQLError("This page does not exist!")

        return flat_page


__all__ = ["FlatPageType", "FlatPageQuery"]
