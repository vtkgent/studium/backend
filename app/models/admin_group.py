# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

import reversion
from django.db import models
from django.utils.translation import gettext_lazy as _

from .base import BaseModel
from .course import Course
from .program import Program
from .user import User


@reversion.register(ignore_duplicates=True)
class AdminGroup(BaseModel):
    """This model represents a custom Studium Group."""

    name = models.CharField(
        max_length=100,
        help_text=_("The name of this admin group. "),
    )

    description = models.TextField(
        help_text=_(
            "The description of this flat page. The description is only used in the "
            "backend for extra information and filtering options. "
        ),
    )

    allowed_courses = models.ManyToManyField(
        Course,
        related_name="allowed_courses",
        blank=True,
        help_text=_("Courses this person is allowed to edit. "),
    )

    allowed_programs = models.ManyToManyField(
        Program,
        related_name="allowed_programs",
        blank=True,
        help_text=_("Programs this person is allowed to edit. "),
    )

    allowed_users = models.ManyToManyField(
        User,
        related_name="allowed_users",
        blank=True,
        help_text=_("Users that are part of this group. "),
    )

    @property
    def uri(self) -> str:
        """Return a unique resource identifier that identifies this admin group."""
        return self.format_uri(self.name)

    def __str__(self):
        return self.name
