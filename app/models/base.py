# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

from django.db import models


class BaseModel(models.Model):
    """An abstract class for models that provides functions to identify objects."""

    class Meta:
        abstract = True

    def __str__(self) -> str:
        """Return a unique resource identifier that identifies this model."""
        return f"<{self.uri}>"

    def fill(self, **kwargs):
        """Set attributes on instance from kwargs."""
        for k, v in kwargs.items():
            setattr(self, k, v)
        return self

    def update(self, **kwargs):
        """Set attributes on instance from kwargs and persists changes to database."""
        return self.fill(**kwargs).save()

    @property
    def uri(self) -> str:
        """Return a unique resource identifier that identifies this model."""
        return self.format_uri(self.pk)

    def format_uri(self, *args):
        """Format the supplied objects into a unique resource identifier."""
        result = f"{type(self).__name__.lower()}"

        if args:
            formatted_args = [
                str(arg).lower() if arg is not None else "null" for arg in args
            ]
            result = f"{result}:{':'.join(formatted_args)}"
        return result


__all__ = ["BaseModel"]
