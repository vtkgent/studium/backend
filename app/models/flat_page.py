# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

import reversion
from django.db import models
from django.utils.translation import gettext_lazy as _

from .base import BaseModel


@reversion.register(ignore_duplicates=True)
class FlatPage(BaseModel):
    """This model represents a flat page. This are pages that are changed frequently."""

    url = models.CharField(
        max_length=255,
        unique=True,
        help_text=_("The url where this flat page will be displayed. "),
    )

    name = models.CharField(
        max_length=255,
        unique=True,
        help_text=_(
            "The name of the flat page, used in the query. ",
        ),
    )

    description = models.TextField(
        help_text=_(
            "The description of this flat page. The description is only used in the "
            "backend for extra information and filtering options. "
        ),
    )

    content = models.TextField(
        help_text=_("The content of the flat page. "),
    )
