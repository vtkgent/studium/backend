# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

import reversion
from django.db import models
from django.utils.translation import gettext_lazy as _

from .base import BaseModel


@reversion.register(ignore_duplicates=True)
class Employee(BaseModel):
    """This model represents someone that works at the university."""

    code = models.CharField(
        max_length=255,
        blank=True,
        unique=True,
        db_index=True,
        help_text=_(
            "The identification code for this employee specified by the university.",
        ),
    )

    name = models.CharField(
        max_length=255,
        db_index=True,
        help_text=_(
            "The name of this employee (e.g. Hennie De Schepper).",
        ),
    )

    def __str__(self):
        """Return this employee's code and name."""
        return f"{self.__class__.__name__}: {self.name}"
