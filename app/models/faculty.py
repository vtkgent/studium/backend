# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

import reversion
from django.db import models
from django.utils.translation import gettext_lazy as _

from .base import BaseModel


@reversion.register(ignore_duplicates=True)
class Faculty(BaseModel):
    """This model represents the faculty a course is taught at."""

    id_code = models.CharField(
        max_length=255,
        unique=True,
        help_text=_(
            "The identification code for this faculty specified by the university "
            "in the course catalogue.",
        ),
    )

    code = models.CharField(
        max_length=255,
        help_text=_(
            "The abbreviation code for this faculty used by students "
            "and the university's main website.",
        ),
    )

    name_en = models.CharField(
        max_length=255,
        help_text=_(
            "The name of this faculty in English (e.g. Faculty of Engineering"
            " and Architecture).",
        ),
    )

    name_nl = models.CharField(
        max_length=255,
        help_text=_(
            "The name of this faculty in Dutch (e.g. Faculteit "
            "Ingenieurswetenschappen en Architectuur).",
        ),
    )

    color = models.CharField(
        max_length=255,
        default="#111b47",
        help_text=_(
            "The color representing this faculty. ",
        ),
    )

    def __str__(self):
        """Return this faculty's code and name."""
        """uri = self.format_uri("ghent-university", self.code)"""
        return f"{self.code} | {self.name_en or self.name_nl}"

    class Meta:
        verbose_name_plural = "Faculties"
