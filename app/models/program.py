# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

import reversion
from django.db import models
from django.utils.translation import gettext_lazy as _

from .base import BaseModel
from .faculty import Faculty


@reversion.register(ignore_duplicates=True)
class Program(BaseModel):
    """This model represents the program at a faculty."""

    code = models.CharField(
        max_length=255,
        unique=True,
        db_index=True,
        help_text=_(
            "The identification code for this program specified by the university.",
        ),
    )

    name_en = models.CharField(
        max_length=1023,
        db_index=True,
        help_text=_(
            "The name of this program in English "
            "(e.g. Joint Section Bachelor of Science in Engineering).",
        ),
    )

    name_nl = models.CharField(
        max_length=1023,
        db_index=True,
        help_text=_(
            "The name of this program in Dutch "
            "(e.g. Joint Section Bachelor of Science in Engineering).",
        ),
    )

    faculty = models.ForeignKey(
        Faculty,
        db_index=True,
        related_name="programs",
        on_delete=models.PROTECT,
        help_text=_("The faculty of this program."),
    )

    education_type = models.CharField(
        max_length=255,
        blank=True,
        help_text=_("Type of education of this program like bachelor"),
    )

    def __str__(self):
        """Return this program's code and name."""
        return f"{self.code} | {self.name_en or self.name_nl} | {self.faculty}"
