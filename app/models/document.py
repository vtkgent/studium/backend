# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

from __future__ import annotations

import hashlib
import os

import reversion
from django.conf import settings
from django.core.validators import (
    FileExtensionValidator,
    MaxValueValidator,
    MinValueValidator,
)
from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

from .base import BaseModel
from .course import Course
from .user import User


def get_document_path(instance: Document, filename):
    return os.path.join(
        "documents/ghent-university/%s/%s/%s"
        % (
            instance.course.programs.first().faculty.code,
            instance.course.programs.first().code,
            instance.course.code,
        ),
        filename,
    )


@reversion.register(ignore_duplicates=True)
class Document(BaseModel):
    """This model represents a single uploaded document."""

    document_extensions = [
        "image/*",
        "application/pdf",
        "text/*",
        "application/gzip",
        "application/zip",
        "application/vnd.ms-powerpoint",
        "application/vnd.openxmlformats-officedocument.presentationml.presentation",
        "application/msword",
        "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
        "application/vnd.ms-excel",
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
        "application/vnd.oasis.opendocument.presentation",
        "application/vnd.oasis.opendocument.spreadsheet",
        "application/vnd.oasis.opendocument.text",
        "application/vnd.rar",
        "application/x-7z-compressed",
        "application/x-bzip2",
        "application/x-latex",
        "application/x-rar-compressed",
        "application/x-tar",
        "application/x-xz",
        "application/x-zip-compressed",
    ]

    # https://fileinfo.com
    py_document_extensions = [
        "7z",  # 7-Zip Compressed File
        "apkg",  # Exported Anki Flashcard Deck
        "bz2",  # Bzip2 Compressed Archive
        "csv",  # Comma-Separated Values File
        "doc",  # Microsoft Word Document (Legacy)
        "docx",  # Microsoft Word Document
        "gif",  # Graphical Interchange Format File
        "gzip",  # Gnu Zipped File
        "jpeg",  # JPEG Image
        "jpg",  # JPEG Image
        "key",  # Apple Keynote Presentation
        "mw",  # Maple Worksheet
        "mws",  # Maple Classic Worksheet
        "odt",  # OpenDocument Text Document
        "pdf",  # Portable Document Format File
        "png",  # Portable Network Graphic
        "ppt",  # Microsoft PowerPoint Presentation (Legacy)
        "pptx",  # Microsoft PowerPoint Presentation
        "rtf",  # Rich Text Format File
        "svg",  # Scalable Vector Graphics File
        "tar",  # Consolidated Unix File Archive
        "tex",  # LaTeX Source Document
        "txt",  # Plain Text File
        "webp",  # WebP Image
        "xls",  # Microsoft Excel Spreadsheet (Legacy)
        "xlsx",  # Microsoft Excel Spreadsheet
        "xz",  # XZ Compressed Archive
        "zip",  # Zipped File
    ]

    course = models.ForeignKey(
        Course, related_name="documents", on_delete=models.CASCADE
    )
    author = models.ForeignKey(User, related_name="uploads", on_delete=models.CASCADE)

    name = models.CharField(
        help_text=_("The name of this document."),
        max_length=255,
        db_index=True,
    )
    page_count = models.IntegerField(
        blank=True,
        null=True,
        validators=[MinValueValidator(0)],
        help_text=_("The amount of pages in this document."),
    )
    download_count = models.IntegerField(
        default=0,
        validators=[MinValueValidator(0)],
        help_text=_("The amount of downloads of this document."),
    )
    upload_date = models.DateTimeField(
        default=timezone.now,
        help_text=_("The time this document was uploaded."),
    )
    last_updated = models.DateTimeField(
        auto_now=True,
        help_text=_("The time this document was last updated."),
    )
    file = models.FileField(
        help_text=_("The path to the file."),
        max_length=255,
        upload_to=get_document_path,
        validators=[FileExtensionValidator(allowed_extensions=py_document_extensions)],
    )

    tags = models.ManyToManyField(
        "DocumentTag",
        related_name="documents",
        help_text=_("The tags assigned to this document."),
    )

    ratings = models.ManyToManyField(
        "User",
        through="DocumentRating",
        related_name="documents",
        help_text=_("The users who gave a rating to this document."),
    )

    anonymous = models.BooleanField(
        default=False,
        help_text=_(
            "If the file is uploaded anonymously and must be hidden in the frontend"
        ),
    )

    @property
    def hash(self) -> str:
        sha = hashlib.sha256()
        sha.update(self.file.path.encode("utf-8"))
        sha.update(self.last_updated.isoformat().encode("utf-8"))
        return sha.hexdigest().lower()

    @property
    def uri(self) -> str:
        """Return a unique resource identifier that identifies this course."""
        return f"{self.course.code}:{self.name}({self.id})"

    @property
    def full_file_path(self) -> str:
        """Return the full file path on the system the document."""
        return settings.BASE_DIR / self.file_path


@reversion.register(ignore_duplicates=True)
class DocumentTag(BaseModel):
    """This model represents something a document can be tagged with.

    Documents can be tagged with e.g. their category.
    """

    name = models.CharField(
        help_text=_("The name of this tag (e.g. summary)."),
        max_length=255,
        unique=True,
        db_index=True,
    )
    parent = models.ForeignKey(
        "self",
        null=True,
        blank=True,
        related_name="children",
        help_text=_("The parent tag of this tag."),
        on_delete=models.CASCADE,
    )

    def __str__(self) -> str:
        """Return this tag's code and name."""
        return f"DocumentTag <{self.name}>"


@reversion.register(ignore_duplicates=True)
class DocumentRating(BaseModel):
    """This model represents a rating a student gave a document."""

    document = models.ForeignKey(
        Document, related_name="document_ratings", on_delete=models.CASCADE
    )
    user = models.ForeignKey(
        User, related_name="document_ratings", on_delete=models.CASCADE
    )

    rating = models.IntegerField(
        default=0,
        validators=[MinValueValidator(0), MaxValueValidator(5)],
        help_text=_("The actual rating given to the document."),
    )


__all__ = ["Document", "DocumentTag", "DocumentRating"]
