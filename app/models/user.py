# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

from __future__ import annotations

from datetime import datetime

import reversion
from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.translation import gettext_lazy as _
from reversion.models import Version

from .flat_page import FlatPage


@reversion.register(ignore_duplicates=True)
class User(AbstractUser):
    """This model represents a single user.

    This model extends the base Django User model with specific UGent CAS
    attributes.
    """

    date_accepted_agreements = models.DateTimeField(
        null=True,
        blank=True,
        help_text=_(
            "When was the last time the user agreed to our Terms of Service and Privacy"
            " Policy?"
        ),
    )

    faculty = models.CharField(
        blank=True,
        max_length=255,
        help_text=_("The faculty the student is enrolled at."),
    )

    university_id = models.CharField(
        blank=True,
        max_length=255,
        help_text=_("The unique id given to a student by the university."),
    )

    last_enrolled = models.IntegerField(
        default=datetime.min.year,
        blank=True,
        null=True,
        help_text=_("The last year a student was enrolled at the university. "),
    )

    @property
    def name(self):
        return f"{self.first_name.strip()} {self.last_name.strip()}"

    def has_accepted_agreement(self, url):
        """If the user has accepted the latest agreement for a given url."""
        # Get the most recent flatpage for a given URL
        flat_page = FlatPage.objects.filter(url=url).first()

        # Make sure a flatpage is present
        # If not the user does not need to accept anything
        if not flat_page:
            return True

        # Get the latest version of the flatpage
        version = (
            Version.objects.get_for_object(flat_page)
            .order_by("-revision__date_created")
            .first()
        )

        # If no version is present, there is no
        # flatpage to agree too.
        if not version:
            return True

        revision_date = version.revision.date_created

        # If the user has not yet accepted any agreements
        # they must accept for the first time
        if not self.date_accepted_agreements:
            return False

        # The last acceptance date must be larger than when the latest revision
        # was created.
        return self.date_accepted_agreements >= revision_date

    class Meta:
        db_table = "auth_user"


__all__ = ["User"]
