# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

from __future__ import annotations

import reversion
from django.db import models
from django.utils.translation import gettext_lazy as _
from reversion.models import Revision

from .base import BaseModel
from .employee import Employee
from .program import Program
from .user import User


def default_course_description():
    description = CourseDescription(content="<p></p>")
    description.save()
    return description.pk


@reversion.register(ignore_duplicates=True)
class CourseDescription(BaseModel):
    """This model represents the description for a specific course."""

    content = models.TextField(
        help_text=_("The description value as a HTML formatted string"),
    )


class CourseDescriptionVersion(BaseModel):
    description = models.ForeignKey(
        CourseDescription,
        blank=True,
        null=True,
        related_name="course_description_revisions",
        on_delete=models.CASCADE,
        help_text=_("Description this revision belongs to."),
    )

    revision = models.ForeignKey(
        Revision,
        blank=True,
        null=True,
        related_name="description_revision_relation",
        on_delete=models.CASCADE,
        help_text=_("Revision this description version belongs to"),
    )

    number = models.IntegerField(
        default=False,
        help_text=_(
            "The version number of this version.",
        ),
    )

    previous_version = models.ForeignKey(
        "CourseDescriptionVersion",
        blank=True,
        null=True,
        related_name="previous_version_relation",
        on_delete=models.CASCADE,
        help_text=_("Version that came before the current version"),
    )

    reverted_version = models.ForeignKey(
        "CourseDescriptionVersion",
        blank=True,
        null=True,
        related_name="reverted_version_relation",
        on_delete=models.CASCADE,
        help_text=_(
            "Version that was reverted to, when the version was created using a revert."
        ),
    )


@reversion.register(ignore_duplicates=True)
class Course(BaseModel):
    """This model represents a single course."""

    code = models.CharField(
        max_length=255,
        unique=True,
        help_text=_(
            "The identification code for this course specified by the university.",
        ),
    )

    name_en = models.CharField(
        max_length=255,
        db_index=True,
        help_text=_("The name of this course in English."),
    )

    name_nl = models.CharField(
        max_length=255,
        db_index=True,
        help_text=_("The name of this course in Dutch."),
    )

    description = models.OneToOneField(
        CourseDescription,
        blank=True,
        on_delete=models.CASCADE,
        help_text=_("The description of this course."),
        default=default_course_description,
    )

    instructor = models.ForeignKey(
        Employee,
        related_name="instructor",
        blank=True,
        on_delete=models.CASCADE,
        null=True,
        help_text=_("The instructors that teach the course."),
    )

    programs = models.ManyToManyField(
        Program,
        through="app.CourseOffering",
        related_name="courses",
        blank=True,
        help_text=_("The programs where this course is taught at."),
    )

    subscriptions = models.ManyToManyField(
        User,
        related_name="subscriptions",
        blank=True,
        help_text=_("The users that subscribed to this course."),
    )

    languages = models.CharField(
        max_length=255,
        blank=True,
        help_text=_("Languages this course is taught in."),
    )

    @property
    def uri(self) -> str:
        """Return a unique resource identifier that identifies this course."""
        return self.format_uri("ghent-university", self.code)

    def __str__(self):
        return f"{self.code} | {self.name_en or self.name_nl}"


@reversion.register(ignore_duplicates=True)
class CourseOffering(BaseModel):
    """This model represents the relation between courses and programs."""

    program = models.ForeignKey(
        Program,
        related_name="offerings",
        on_delete=models.CASCADE,
        help_text=_("The program where the course is offered."),
    )

    course = models.ForeignKey(
        Course,
        related_name="offerings",
        on_delete=models.CASCADE,
        help_text=_("The course that is offered."),
    )

    semester = models.CharField(
        max_length=10,
        blank=True,
        help_text=_("The semester of the year that the course is taught at."),
        choices=[("1", "1st"), ("2", "2nd"), ("J", "Year")],
    )

    year = models.CharField(
        max_length=2,
        blank=True,
        help_text=_(
            "Full-time trajectory. This field shows which model track (master year) "
            "a course belongs to. 1 = 1st master year, 2 = 2nd master year, "
            "not filled in = the student chooses when to take the course."
        ),
    )

    credits = models.DecimalField(
        blank=True,
        null=True,
        max_digits=3,
        decimal_places=1,
        default=False,
        help_text=_(
            "The amount of credits",
        ),
    )

    def __str__(self):
        """Return this relation's course and tag."""
        return self.format_uri(self.program, self.course)


__all__ = [
    "Course",
    "CourseDescription",
    "CourseDescriptionVersion",
    "CourseOffering",
]
