# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

from .course import Course, CourseDescription, CourseDescriptionVersion, CourseOffering
from .document import Document, DocumentRating, DocumentTag
from .employee import Employee
from .faculty import Faculty
from .program import Program
from .user import User
from .flat_page import FlatPage
from .admin_group import AdminGroup

__all__ = [
    "Course",
    "Employee",
    "Faculty",
    "Program",
    "CourseOffering",
    "Course",
    "CourseDescription",
    "CourseDescriptionVersion",
    "Document",
    "DocumentTag",
    "DocumentRating",
    "User",
    "FlatPage",
    "AdminGroup",
]
